$conf = $conf.recursive_merge({
	:debug => false,
	:license => false,
	:exec_params => {
	    :erl_exe => 'c:\Program Files\erl5.7.5\bin\erl',
	    :erl_sh => 'erl',
	    :node_name => 'astrakhan@dtf24.ru',
	    :node_cookie => 'astrakhan',
	    :node_title => 'astrakhan'
	    }
	})
