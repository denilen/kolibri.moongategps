SRC_PATH:= -pa src -pa ebin -pa deps/*/ebin
ERL_RUN_ARGS:= $(SRC_PATH) -sname moongate -s moonaget_app main

compile:
	rake

.PHONY: rebar
rebar:
	./rebar -j 3 compile


update:
	git fetch
	git reset origin --hard
	$(MAKE) compile

get-deps:
	@./rebar -j 3 get-deps

clean:
	@./rebar clean
	@rm -f erl_crash.dump
	@rm -f test/*.beam

clean-beam:
	@rm -f ebin/*.beam

d:
	dialyzer --src -I include src

d_beam:
	dialyzer ebin

.PHONY: test
test:
	@make clean-beam > /dev/null
	REBAR_TEST=1 ./rebar compile omen_comet
	ct_run -spec tests.spec -cover cover.spec $(SRC_PATH)
	@make clean-beam > /dev/null
	make tshow

test-clean:
	@rm -f -R test/log/ct_run.ct@*

tshow:
	lynx test/log/index.html -nopause -use_mouse
