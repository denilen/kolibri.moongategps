-module(test).
-compile(export_all).

-include("logger.hrl").

-include_lib("./common.hrl").
-include_lib("./config.hrl").

timeshit()->
	time2usecs(str2time("2020-10-10 00:00:00.000")).

timeshit2()->
	cursecs().

get_driver(Id) ->
	[D] = ets:lookup(drivers_auth, Id),
	D.

get_rule() ->
	get_rule("2290290").

get_rule(Rule) when is_integer(Rule) ->
	get_rule(integer_to_list(Rule));

get_rule(Id) ->
	[{_, R}] = ets:lookup(rules, Id),
	R.

get_order(Id) ->
	case ets:lookup(orders, Id) of
		[{ _, _, Order }] -> Order;
		[] -> undefined
	end.

get_rule_by_order(Order) ->
	cache:get_rule(Order#order.rule_head).

get_drivers_id_for_parking(Parking) ->
	[ element(2, X) || X <- driver:only_free_online(cache:get_parkings_drivers(Parking)) ].

get_orders_for_parking(Parking) ->
	[{ _, Order, _}] = ets:lookup(drivers_distrib, Parking),
	[ element(2, X) || X <- Order ].

get_run_orders() ->
	[ X || X <- [ element(7,State) || {_, Pid, State, _} <- ets:tab2list(drivers_online), is_pid(Pid) ], is_number(X), X > 0 ].

select_drivers(OrderId, DriversId) when is_list(DriversId) ->
	Order = get_order(OrderId),
	Drivers = [ get_driver(D) || D <- DriversId ],
  orders:select_driver(Order, cache:get_rule(Order#order.rule_head), Drivers).

code_update() ->
	[ case code:soft_purge(M) of
			true ->
				io:format("~p - ok\n", [ M ]),
				code:load_file(M);
			false ->
        io:format("~p - fail\n", [ M ]),
				{ error, not_purge, M }
	  end || M <- erlang:loaded(), not(code:is_sticky(M)) ].
