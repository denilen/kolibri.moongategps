connect_string() -> config:get(database_dsn).
sort_zones_by_id() -> config:get(sort_zones_by_id).	%% сортировать ли стоянки  по айдишникам; по алфавиту в противном случае.
show_all_drivers() -> config:get(show_all_drivers).	%% показывать клиенту всех водителей в распределении или только своих.
econom_logic() -> config:get(econom_logic).		%% отрабатывать ли специальную логику для эконома. true|false
predvar_time() -> config:get(predvar_time).		%% minutes - за какое время до выполнения активировать предварительный заказ
port() -> config:get(port).

%% Client notification
client_preliminary_notification_method() -> config:get(client_preliminary_notification_method).	%% sms_phone|sms_no_phone|call|off  Метод предварительного (при взятии заказа) оповещения.
client_missing_notification_method() -> config:get(client_missing_notification_method).		%% sms_phone|sms_no_phone|call|off  Метод приглашения клиента.

%% Лицензирование
key() -> config:get(lic_key).	%% имя запрашиваемого ключа лицензии
