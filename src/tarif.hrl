-type(tarif_type() :: none | fixed | distance | timed_minute | timed_hour | complex).

-record(tarif, {
  type :: tarif_type(),
  properties :: proplists:proplist() %% Остальные свойства тарифа.
  %% Список свойств:

    %% обязательные:
    %%    pause_time :: integer(), %% Граничное время простоя во время движения (остановки в пробках), сек
    %%    pause_cost :: integer(), %% Cтоимость простоя после граничного времени простоя во время движения, руб/мин
    %%    wait_time :: integer(), %% Граничное время во время ожидания клиента, мин
    %%    wait_cost :: integer(), %% Стоимость простоя после граничного времени ожидания, руб/мин
    %%    baggage_cost :: integer(), %% Стоимость перевозки багажа

    %% В зависимости от типа тарифа:

    %% 0. none - нет тарифа
    %% 1. fixed - фиксированный
    %%    full_cost :: integer(), %% Полная стоимость поездки
    %% 2. distance - покилометровый
    %%    cost_before :: integer(), %% Стоимость поезди до distance_before км
    %%    distance_before :: integer(), %% Минимальное расстояние поездки
    %%    cost_after :: integer() %% Стоимость 1 км после distance_before
    %% 3. timed_minute - поминутный
    %%    cost_landing :: integer() - стоимость посадкиб, руб
    %%    cost_minute :: integer() - стоимость 1 минуты, руб
    %% 4. timed_hour - почасовой
    %%    cost_landing :: integer() - стоимость посадки, руб
    %%    cost_hour :: integer() - стоимость 1 часа, руб
    %% 5. complex - комбинированный
    %%    cost_before :: integer(), %% - стоимость 1 минуты до достижения distance_before, руб
    %%    distance_before :: integer(), %% - граничное расстояние поездки, км
    %%    cost_after :: integer(), %% - стоимость 1 минуты после достижения distance_before
}).
