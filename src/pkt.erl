-module(pkt).
-author("denilen").

-include("logger.hrl").

-export([
	auth_error/2,
	parkings_list/2,
	parkings_info/2,
	balance/2,
	order_offer/4,
	order_confirm/5,
	order_not_confirmed/3,
	order_cancelled/3,
	message/2,
	finitor/1,
	ok/1, fail/3, fail/4,
	free_orders/1
]).

-include_lib("./common.hrl").
-include_lib("./config.hrl").

wrap_load(Head, ServerOpcode, Load) ->
  lager:debug("Send packet ~p ~p ", [ ServerOpcode, Load ]),
	<<Head/binary, ServerOpcode:8, (size(Load)):16/little, Load/binary, <<170:8,170:8>>/binary>>.

auth_error(Head, Reason) ->
	wrap_load(Head, 1, <<(auth_error_reason2int(Reason)):8>>).

auth_error_reason2int(bad_pass)->0;
auth_error_reason2int(bad_version) ->1;
auth_error_reason2int(double_login) ->2;
auth_error_reason2int(bad_badmatch) ->3.


parkings_list(Head, Driver) ->
	Parkings = cache:get_parkings(),
  ParkingsBinary = << <<Id:16/little-unsigned-integer,<<(list_to_binary(Name))/binary, 0:80>>:10/binary>> || {Id, Name} <- Parkings>>,
  AutoAppointment = case driver:attr_has_autoappointment(Driver) of
      true -> 1;
      false -> 0
    end,

	wrap_load(Head, 2, <<
    AutoAppointment:8,
    ParkingsBinary/binary
  >>
		).

parkings_info(Head, State) ->
	Distrib = cache:get_drivers_distrib(),
	DriverService = (State#state.driver)#driver.serviceid,
	wrap_load(Head, 3,
				list_to_binary(lists:map(fun({Parking, OrdersNum, Drivers}) ->
								DriversFiltered = case show_all_drivers() of
														 true -> [Driver#driver.id
															 || Driver <- Drivers];
														 _ -> [Driver#driver.id
															 || Driver <- Drivers, Driver#driver.serviceid =:= DriverService]
													 end,
								<<(if
									 State#state.parking =:= Parking -> 1;
									 true -> 0
								 end):8, 
								Parking:8,
								(length(DriversFiltered)):8,
								OrdersNum:8,
								(<< <<DriverId:16/little>> 
									|| DriverId <- DriversFiltered>>)/binary>>
										 end
										 , Distrib))) .

balance(Head, Balance) ->
	case Balance of
		null -> 
			lager:error("sending balance failed: null balance~n",[]),
			<<>>;
		_ ->
			BalBin = erlang:list_to_binary( erlang:integer_to_list(Balance)),
			wrap_load(Head, 8,
				<<(size(BalBin)):8,
					BalBin/binary>>)
		end.


order_offer(Protocol, Head, Order, Preview) ->
  ResBin = binary:list_to_bin(Order#order.addrfrom ++" "++ Order#order.housefrom ++" "++ Order#order.flatfrom),
  AddrBin = <<" ">>,
  CommentBin = binary:list_to_bin("Служба: " ++ Order#order.rule_head ++ " " ++ Order#order.descr),

	Secs = time2usecs(str2time(Order#order.dtarrive)),
	OrderState = case Preview of
					 false -> 0;
					 true -> 5
				 end,
	DriverAnswer = 0,
  { GpsX, GpsY, GpsZ } = { 0, 0, 0 },
  << Phone:11/bytes, _/binary >> = <<"                           ">>,
	wrap_load(Head, 7,
				<<(size(ResBin)):24/big-unsigned-integer,
				ResBin/binary,
				(size(AddrBin)):24,
				AddrBin/binary,
				0:24,
				OrderState:4,
				DriverAnswer:4,
				Secs:32,
				(size(CommentBin)):24,
				CommentBin/binary,
				0:16,
				(Order#order.id):32,
        Phone/binary,
        (gps_to_binary(GpsX)):32,
        (gps_to_binary(GpsY)):32,
        (gps_to_binary(GpsZ)):32,
        (tarif:to_binary(Protocol, Order#order.tarif))/binary
        >>
  ).

order_confirm(Protocol, Head, Order, A, TimeTakenSecs) ->
	[ResBin, AddrBin, CommentBin] = 
		lists:map(fun erlang:list_to_binary/1, 
				[Order#order.addrfrom ++" "++ Order#order.housefrom ++" "++ Order#order.flatfrom, 
					 Order#order.addrto,
					 "Служба: " ++ Order#order.rule_head ++ " " ++ Order#order.descr ++ " тел." ++ Order#order.phone ++ " Сумма:" ++ erlang:integer_to_list(Order#order.sum)]),

	OrderState = case A of
					 refuse -> 1;
					 not_confirmed -> 3;
					 _ -> 2
				 end,
	DriverAnswer = time2code(A),
	% SecsArrive = time2usecs(str2time(Order#order.dtarrive)),
  { GpsX, GpsY, GpsZ } = Order#order.gps_from,
  << Phone:11/bytes, _/binary >> = << (list_to_binary(Order#order.phone))/binary, <<"           ">>/binary >>,
	wrap_load(Head, 7,
				<<(size(ResBin)):24,
				ResBin/binary,
				(size(AddrBin)):24,
				AddrBin/binary,
				0:24,
				OrderState:4,
				DriverAnswer:4,
				%SecsArrive:32,
				TimeTakenSecs:32,
				(size(CommentBin)):24,
				CommentBin/binary,
				0:16,
				(Order#order.id):32,
        Phone/binary,
        (gps_to_binary(GpsX)):32,
        (gps_to_binary(GpsY)):32,
        (gps_to_binary(GpsZ)):32,
        (tarif:to_binary(Protocol, Order#order.tarif))/binary
        >>).

order_cancelled(Protocol, Head, Order) ->
	order_confirm(Protocol, Head, Order, refuse, cursecs()).

order_not_confirmed(Protocol, Head, Order) ->
	order_confirm(Protocol, Head, Order, not_confirmed, cursecs()).

message(Head, Mess) when is_record(Mess, message) ->
	Bin = list_to_binary(Mess#message.text),
	wrap_load(Head, 9,
				<<(a2msgType(Mess#message.type)):8,
				(size(Bin)):8,
				Bin/binary>>).

finitor(Head) ->
	wrap_load(Head, 10, <<>>).

ok(Head) ->
	wrap_load(Head, 11, <<255>>).

fail(Head, ErrorCode, ErrorMsg) when is_list(ErrorMsg) ->
	fail(Head, ErrorCode, list_to_binary(ErrorMsg));

fail(Head, ErrorCode, ErrorMsg) when is_atom(ErrorCode) ->
	fail(Head, atom_to_binary(ErrorCode, utf8), ErrorMsg);

fail(Head, ErrorCode, ErrorMsg) when is_binary(ErrorMsg), is_binary(ErrorCode) ->
	Body = <<
		(byte_size(ErrorCode)):16/big-unsigned-integer,
		ErrorCode/binary,
		(byte_size(ErrorMsg)):16/big-unsigned-integer,
		ErrorMsg/binary
	>>,
	lager:error("Operation fail with ~p:~s", [ ErrorCode, binary_to_list(ErrorMsg) ]),
  wrap_load(Head, 12, Body).

fail(Head, ErrorCode, FormatMsg, Param) ->
	Msg = io_lib:format(FormatMsg, Param),
	fail(Head, ErrorCode, list_to_binary(Msg)).

pause_time(Head, Time, LastPause) ->
  Body = <<
    (max(0, Time)):32/big-signed-integer,
    (max(0, if
	    is_integer(LastPause) -> LastPause;
	    true -> 0
    end)):32/big-unsigned-integer
  >>,
  wrap_load(Head, 13, Body).

free_orders(Head) ->
	GpsList = [ B || B <-
		lists:map(fun(Order) ->
			case Order#order.gps_from of
				{ X, Y, _Z } ->
					<< (gps_to_binary(X)):32/big-signed-integer,
						 (gps_to_binary(Y)):32/big-signed-integer
					>>;
				_ -> undefined
			end
		end, cache:free_orders()),
	is_binary(B) ],
	Body = <<
		(length(GpsList)):32/big-unsigned-integer,
		(list_to_binary(GpsList))/binary
	>>,
  wrap_load(Head, 14, Body).
