-module(db).
-author("denilen").

-include("logger.hrl").

%% API
-export([
	q/2, qp/3, update/6, start/0, connect/0
]).

-export([
	handle_call/2
]).
-include("config.hrl").

start() ->
  odbc:start().

connect() ->
  odbc:connect(connect_string(), [
    %%{binary_strings, on}
  ]).

q(Connection, Query) ->
  %%lager:info("DB query ~p", [ Query ]),
  case odbc:sql_query(Connection, Query) of
    {error,"No SQL-driver information available."} -> { updated, 0 };
    Result -> Result
  end.

qp(Connection, Query, Param) ->
  %%lager:info("DB query param ~p ~p", [ Query, Param ]),
  case odbc:param_query(Connection, Query, Param) of
    {error,"No SQL-driver information available."} -> { updated, 0 };
    Result -> Result
  end.

update(Connection, Table, SetSection, SetParam, WhereSection, WhereParam) ->
  qp(Connection,"UPDATE "++ Table ++ " SET " ++ SetSection ++ " WHERE " ++ WhereSection, SetParam ++ WhereParam).


%% ---------------------------------------------------------------------------------------------------------------------
%% напюанрвхй CALL SQL ЯЛ sql.erl
%% ---------------------------------------------------------------------------------------------------------------------

handle_call({ query, Query }, Conn) ->
  q(Conn, Query);

handle_call({ query_param, Query, Param }, Conn) ->
	qp(Conn, Query, Param);

handle_call({ update, Table, SetSection, SetParam, WhereSection, WhereParam}, Conn) ->
	update(Conn, Table, SetSection, SetParam, WhereSection, WhereParam).
