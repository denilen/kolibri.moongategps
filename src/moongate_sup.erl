-module(moongate_sup).
-author("denilen").

-include("logger.hrl").

%% Главный супервизор приложения.

-behaviour(supervisor).

-export([start_link/1, init/1]).

-spec start_link(Config :: string()) -> {ok, pid()} | ignore | {error, term()}.
start_link(Config) ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, [Config]).

init(ConfigFile) ->
  { ok, {{one_for_one, 500000000, 1000}, [
	  { config, { config, start_link, [ConfigFile] }, permanent, 5000, worker, [config] },
	  { tcp_server, { canal, start_link, [] }, permanent, 5000, worker, [canal] }
  ]}}.
