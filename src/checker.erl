-module(checker).
-author("denilen").

-include("logger.hrl").

-ifdef(debug).
-compile(export_all).
-else.
-export([check/0]).
-endif.

-include_lib("./local.hrl").

sites() -> ["lic1.jetmon.ru", "lic2.dtf24.ru", "lic3.dtf24.ru", "lic4.dtf24.ru", "lic5.dtf24.ru"].

-ifdef(debug).
-define(URLS, ["https://moongate/lic.php?key="++key()]).
-else.
-define(URLS, ["https://"++S++"/lic.php?key="++key()||S <- sites()]).
-endif.

check()->
	inets:start(),
	ssl:start(),
	check_policies(?URLS).

check_policies([]) -> nogood;
check_policies([Url|Rest]) ->
	case check_ip(Url) of
		nogood ->
		    check_policies(Rest);
		good -> 
			case get_policy(Url) of
				P={N,B1,B2} when is_integer(N), is_boolean(B1),is_boolean(B1) -> P;
				_ -> check_policies(Rest)
			end
	end.

get_policy(Url) ->
	catch parse_policy(get_page(Url)).
	
-ifdef(debug).
-define(SSL_OPTS, [{ssl, [{verify, 2}]}]). 
-else.
-define(SSL_OPTS, [{ssl, [{verify, 2}]}]). 
-endif.

get_page(Url) ->
	httpc:request(get, {Url, []}, [], []),  % не понятно пока почему, но перед запросом лицензии приходиться делать пустой запрос, иначе не работает
	case httpc:request(get, {Url,[]}, ?SSL_OPTS, [{full_result, false}])
	of
		{ok, {RetCode, Result}} when RetCode>=200,RetCode<300 ->
			take(keysize() div 8, Result);
		{ok, {RetCode, _}} ->
			lager:error("Can't get license: ~s ; http code:~p~n", [Url, RetCode]),
			fail;
		{error, Why} ->
			lager:error("Can't get license: ~s : ~w~n", [Url, Why]),
			fail
	end.

%% verifyFun =  fun([]) -> 
%% 					 true; 
%% 				([_| _]) -> 
%% 					 false 
%% 			 end.


parse_policy(fail) -> nogood;
parse_policy(Page) ->
	readLic(unPKCS(dec(Page))).

-ifdef(debug).
check_ip(Url) -> good.
-else.
check_ip(Url) ->
	case inet:gethostbyname(
		   lists:takewhile(
			 fun(C) -> 
					 C=/=$/ 
			 end, lists:nthtail(8,Url))
		   ,inet) of
		{ok, {hostent,_,_,_,_,Ips}} ->
			good_ips(Ips);
		_ -> nogood
	end.
-endif.

good_ips([]) -> nogood;
good_ips([Ip|Rest]) ->
	case Ip of
		{148,251,185,91} -> good;
		_ -> good_ips(Rest)
	end.

%rsa decoding and lic reading------------------------------------------------------------

mxp(_,0,_) -> 
	1;
mxp(A,B,C) -> 
	if B rem 2>0
	   ->A;
	   0<1 -> 1 
	end 
		* mxp(A*A rem C,B div 2,C) rem C.

%mod() -> "00a291c2b64b1b4e42ea3e9dce8a03282f4f3efe0e44139af8eaa6004a4c1e916ed94a29480dc7288eb5726848f72776c143547896374df07b435cf5361112aaaddc492af7ca30387f4e56b10359e6fdca6a71e2dca2a9c0c8c88e3cd8e135c2750bcc83fdece582e511328abb6d87ccbb3218fc9f6f1d193dafe3e55583ba5114650a7bce90c93496f85a08a1b04a0ced00d611834ee1cfcb28357ce44f808c48db4e5eb354d349b9d7224997d817b0870a2eb1a1be60a86d7cc11acc84ff4782aa5005648a690a934b9a287facdaf5ba85d77db14059b33afe99da7c59aa40f9d8ffc120b4344c161f770484064b98c2a9be68291cc6866acd9a024e7d1e0a9991afb816c873563b06ce56a04c078052182f21355e1b983bb9fe2bb57265291646643a377f55c01b5b3fb10819b09d1e37ac3048715cb14f2d3f74ba23179752d090ec8a310861009d3d3d4f4ade60ea2c08fb3664eb0c266edc9363da4d290166ad359416e852f5fbf53cac629a32fcf5d804cdc7a7a5faae250f7156d20dbb297d71c2e4999ecabc24acebb9b6d114eae3cf550db530723b70c4a08256331d374645d055d05c7569556f9ebb8a3bec6d2adaaed43a8eff99fd3bf2b388439a6a31518d01a267c7e4321ef69729da4182f71f9a923d1a9f9eec6b7cb28a5c192eaf894e70fd9c13e69511ccceaf9b56c3af9c4b37b427abcb2a62218696cbbb",

mod() -> 
	lists:zipwith(fun(C1,C2)->
						  lists:nth((string:chr("0123456789abcdef", C1)-1) bxor (string:chr("0123456789abcdef", C2)-1)+1, "0123456789abcdef")
				  end, 
				  "a2297bae9ef7045ae4878edd48885707eec50566b9e6f5470c27023d340c32ea4d734b8fab57a716e8f0817f483911be7dfc0f070768d5e6847dd2cdc2dc8d8121f498c8944843341aaf56891c483f85db62c089fc4417c088eee0c181aa164ffce24e34fce717a83a9aeaed725903fa5ae3472db6f075b641ec7cf8bf59653e495ecf6919204a75b767cea9ff8aee02abd49e1552bba251b695ab504242b3aa6adcb6a107ae70431bc6376abdff350702502e95b2cc082b2a7776c0069952f2484e8b660420af4870890545d115506c421b7983ab8379160dd51d5d170c2527d66057e6455614f66f7be1bd1ed057260a6c50c57d6614389ba5be2872b00ff51421b39de2a648b367db5012934bbf6eb8f4f4f92b8c1ce6d4fcae44724868a56632f1211f16d357dd822a62568290047b13be0ee91701b40300fb99f34b624683a574ff928b20f7199050a304434dbdecb92741f46472d3e1ec460fdcf21526739f63377776d9be14a6d04accdf1ee0a3ba08bd00d33eb009b233d13297def788c96788780e2847aa42cc0ffdbe83c725c17f71a72f610718dfae5935d59cc5117f930c6e60f61b53e6e3c707681a092189aa5e21812bd1dada1e3e4852cec395e5d5618c996858c03ffcdb9a9d05910695f5ef90745ce322139c54def3e9076adf0941aa961c6b49ceea698f0000c45dde0325f904678067382b3297ae948fb7",
				  "a2909b1247568fabb961530b3f766fb775b48700bedeb583917ca99b88ffe4cc9ebaad967bb5b8dc6c0c19aee3961b6ccddf6d12041819cae9548d9fa34820bfadb7e0f14e66d0445a02fc4788c624b6af1e49417fd945ccf22acfb1fb4587aee88a1501844daed1396d38299fe9c3630fcc74787e24333eadd3204842813d1c1b2a0a349a11113eeaa2247976d2340fddd19175d74c57d522878d4b1f089b58478f041ab0742858d30e65a11ffd6b6913c793af2f37befe14e8a90715c7e5c22a7ee10b74ddcb4dbcaca4a90f157e6c9ae3e82341f43790933102b77ea5e247e52e46980390e7abeba5bfcc86bb6be43c266618c2b839c23f24ad6e951b0e8fc18ca99422e2ef8fc500c803a04cb76f1b42fc8576ea118290ba7cd0aa3d87d94ddf258d369a0e0c7be4f6e7ee693282aa5c5886d0983ea18ee23fc449f6bc3400b7ec28865d058ee1dae3681624a2c8cf78c9d25ff480418780c86311df44e5b4aa6335a1f55dca47951e2f9c22971d1c41c47b204ac877b2e6a88917655c2b9d025c05d76d55cf21371383e450d4451e6c49a4472521e8d8aa11bc17dac283035f495ee52a9019db6f17ed2ba516554f9c851e4c8aacba2bb3766087953549ccf5ccd8386fb7c40d94ad883a752c5445c75826c3ad259c774f5a1c3a4024ae19ed22601196b04d175ef0f034a29b0b22b61b285f394afd90db573ee7e3c317a6").

keysize() -> 4096.

dec(M) ->
	IM = http_util:hexlist_to_integer(hex:bin_to_hexstr(list_to_binary(M))),
	I = mxp(IM,65537,http_util:hexlist_to_integer(mod())),
	hex:hexstr_to_bin(	  string:right(
		http_util:integer_to_hexlist(I), keysize() div 8*2,$0)).



unPKCSh(<<Q:8,Rest/binary>>) when Q =/=0 -> unPKCSh(Rest);
unPKCSh(<<Q:8,Rest/binary>>) when Q =:=0 -> Rest;
unPKCSh(<<>>) -> <<>>.	

%PKCS#1
unPKCS(P)->
	<<0:8,2:8,R/binary>> = P,
	unPKCSh(R).

readLic(D)->
	[A,B,C|R]=string:tokens(binary_to_list(D), ";\n"),
	[MaxDrivers,EnableCap,EnableLic] = lists:map(fun erlang:list_to_integer/1, [A,B,C]),
	{MaxDrivers, int2bool(EnableCap), int2bool(EnableLic)}.

int2bool(1) -> true;
int2bool(0) -> false.

takeh(0,_,L) -> L;
takeh(N, [H|R], L) -> takeh(N-1, R, [H|L]).	

take(N, List) ->
	lists:reverse(takeh(N, List, [])).

