-module(canal).
-author("denilen").

-compile(export_all).

-include("logger.hrl").

-include_lib("./common.hrl").
-include_lib("./config.hrl").


-define(PAUSE_TICK, 5).

-ifdef(lic).
-define(TV, ok).
-else.
-define(TV, tv:start()).
-endif.

start_link() ->
	Pid = spawn_link(?MODULE, tcp_server_start, []),
	{ ok, Pid }.

tcp_server_start() ->
	Pid = spawn_link(cache, start, [self()]),
	receive
		start_ok -> ok;
		{'EXIT', Pid, Why} ->
			lager:error("cache start fail: ~p", [Why]),
			io:format("cache start fail: ~p", [Why]),
			erlang:exit({cache_start_fail, Why})
	end,

	erlang:register(canal, self()),
	socket_open(15).

socket_open(0) ->
	io:format("Cant binding listen socket. Exit"),
	lager:error("Cant binding listen socket. Exit"),
	exit(port_not_open);

socket_open(N) when N > 0 ->
	case gen_tcp:listen(port(), [binary
								% inet,
								% {ip, "127.0.0.1"},
								,{backlog, 1000}
								]) of
		{ok, LSock} ->
			io:format("Listening on port: [ ~p ]",[port()]),
			lager:info("Listening on port: [ ~p ] : ~p",[port(), LSock]),
			process_flag(trap_exit, true),
			?TV,
			?MODULE:tcp_server_loop(LSock);
		{error, Why} ->
			lager:error("Binding listen socket on port: [ ~p ] fail: ~p", [port(), Why]),
			io:format("Binding listen socket on port: [ ~p ] fail: ~p. ~p attempts remained.", [port(), Why, N-1]),
			timer:sleep(10*1000),
			?MODULE:socket_open(N-1)
	end.


tcp_server_manage_message()->
	receive 
		{'EXIT', _, Reason} when 
				Reason =:= normal;
				Reason =:= auth_failed;
				Reason =:= tcp_closed;
				Reason =:= tcp_error;
				Reason =:= client_timeout;
				Reason =:= disconnect ->
			ok;
		{'EXIT', WorkerPid, Reason} -> 
			case extreme_cleanup(WorkerPid) of
				no_pid -> lager:error("got unknown exit, reason: ~p", [Reason]);
				_ -> lager:error("got tcp_worker_loop exit, reason: ~p",[Reason])
			end,
			ok;
		{tcp_closed, _Sock} ->
			lager:info("tcp socket closed prematurely",[]),
			ok
	after 0 ->
			all
	end.

tcp_server_manage_messages()->
	case tcp_server_manage_message() of
		ok -> tcp_server_manage_message();
		all -> ok
	end.


tcp_server_loop(LSock) ->
	case gen_tcp:accept(LSock) of
		{ok, Sock} ->
			Ref = erlang:make_ref(),
			Pid = spawn_link(fun() ->
								receive 
									{permission, Ref} ->
										inet:setopts(Sock, [binary, 
															{active, true},
															{packet, 0},
															{keepalive, true}
															%%,{nodelay, true}
															 ]),
										%%pman:proc(self()),
										?MODULE:tcp_worker(Sock, #state{}, <<>>);
									{no_socket, _Reason} ->
										ok										
								end
						end),
			case gen_tcp:controlling_process(Sock, Pid) of
				ok -> Pid ! {permission, Ref};
				{error, Reason} -> Pid ! {no_socket, Reason}
			end;
		E -> 
			lager:error("accepting socket error: ~p",[E])
	end,				
	tcp_server_manage_messages(),
	?MODULE:tcp_server_loop(LSock).

worker_finalize(Sock, State, Reason) ->
	cleanup_disconnected(State),
	case Sock of
		undefined -> ok;
		_ -> gen_tcp:close(Sock)
	end,
	exit(Reason).

resend_order(Pid, Order, Time, WhenTaken, Mode) ->
		Ref = make_ref(),
		Pid ! {resend_order, Mode, Ref, self(), Order, Time, WhenTaken},
	  receive
		  { Ref, Status } -> Status
		after 5000 ->
			{ error, timeout }
	  end.

tcp_worker(Sock, State, Rest) ->
	tcp_check_timer(),
	tcp_worker_loop(Sock, State#state{
		last_tcp = cursecs()
	}, Rest).

tcp_worker_loop(Sock,State,Rest) ->
	Result =
		receive
			disconnect ->
				if State#state.orderid =:= 0 ->
						log(State, "closing connection",[]),
						worker_finalize(Sock, State, disconnect);
					 true ->
						log(State, "Disconnect forced for Driver while on order; ignoring"),
						{State, Rest}
				end;

			{new_auth, Driver} -> %% Обновление инфы о водителе (вызывается из cache) canal:refresh_info
				NState = case driver:get_attr(Driver) =/= driver:get_attr(State#state.driver) of
					true ->
						gen_tcp:send(Sock,
							pkt:parkings_list(<<170:8, 9:16/little,
									 ((State#state.driver)#driver.id):16/little>>, Driver)),
						State#state { parking = 0 };
					false -> State
				end,
				case {(State#state.driver)#driver.state, Driver#driver.state, State#state.orderid} of
					{work, cant_work, 0} ->
						cache:do_unregister_driver(NState#state.parking, NState#state.driver),
						gen_tcp:send(Sock,
									 pkt:message(<<170:8, 9:16/little, 
													 ((NState#state.driver)#driver.id):16/little>>,
												 #message{id=0,
														 text="Снят со смены.",
															type = leader})),
						{NState#state{name = negative_balance, parking=0, driver = Driver}, Rest};
					{cant_work, work, _} ->
						gen_tcp:send(Sock,
									 pkt:message(<<170:8, 9:16/little, 
													 ((NState#state.driver)#driver.id):16/little>>,
												 #message{id=0,
															text="На смене.",
															type = message})),
						{NState#state{name = unreg, parking =0, driver = Driver}, Rest};
					_ ->
						{NState#state{driver = Driver}, Rest}
				end;
			auth_err ->
				case inet:peername(Sock) of
					{ok, {Addr, Port}} ->
						lager:info("Auth failed; closing connection from ~p:~p",[Addr, Port]);
					_ -> 
						lager:info("Auth failed; closing connection from unknown",[])
				end,
				worker_finalize(Sock, State, auth_failed);
			{tcp_closed, Sock} ->
				log(State, "socket closed by client"),
				worker_finalize(Sock, State, tcp_closed);
			{tcp_error, Sock, Reason} ->
				log(State, "tcp error:~p", [Reason]),
				worker_finalize(Sock, State, tcp_error);
			Mess when is_record(Mess, message) andalso State#state.lastMessage =:= no_message ->
				log(State, "sending message message: ~p",[ Mess#message.id ]),
				gen_tcp:send(Sock,
							 pkt:message(<<170:8, 9:16/little, 
											 ((State#state.driver)#driver.id):16/little>>, Mess)),
				{State#state{lastMessage = Mess},Rest};
			{offer, Order, Token} ->
				NewSt = case lists:keyfind(Order#order.id, 1, State#state.offers) of
								 false ->
									 case (cursecs() - row_timeout(State#state.driver)) > State#state.lasttake andalso State#state.orderid =:= 0 andalso ( Order#order.parking =:= State#state.parking orelse driver:attr_has_autoappointment(State#state.driver)) of
										 true ->
											 log(State, "sending offer for Order: ~p ",[Order#order.id]),
											 Res=gen_tcp:send(Sock,
															pkt:order_offer(State#state.client_protocol, <<170:8, 9:16/little,
																				((State#state.driver)#driver.id):16/little>>,
																			Order, false)),
											 log(State, "sending offer for Order: ~p resulted in ~p",[Order#order.id, Res]),
											 State#state{offers = [{Order#order.id, Token} | State#state.offers]};
										 false ->
											 log(State, "sending offer refused Order: ~p ", [Order#order.id] ),
											 State
									 end;
								 _ ->
									 lager:warning("order tried to double offer; Order: ~p",[Order#order.id]),
									 State
							 end,
				{NewSt, Rest};
			%% passive remove by timeout
			{remove_offer, Order} ->
				case lists:keyfind(Order#order.id, 1 ,State#state.offers) of 
					false -> 
						{State, Rest};
					_ -> 
						cache:do_reregister_driver(State#state.parking, State#state.parking, State#state.driver),
						{State#state{offers = 
									 [{OrderId, Token} || {OrderId, Token} <- State#state.offers, OrderId =/= Order#order.id]},
						 Rest}
				end;

			{resend_order, Mode, Ref, FromPid, Order, Time, WhenTaken} when Mode =:= forced orelse (State#state.status =:= free andalso State#state.orderid =:= 0) orelse (State#state.orderid =:= Order#order.id) ->
				lager:info("sending order confirmation: Driver: ~p  Order: ~p",[(State#state.driver)#driver.id,Order#order.id]),
				gen_tcp:send(Sock,
							 pkt:order_confirm(State#state.client_protocol, <<170:8, 9:16/little,
												 ((State#state.driver)#driver.id):16/little>>, 
												 Order, Time, WhenTaken)),
				NewSt = State#state{
					orderid = Order#order.id,
					status = working
				},
				cache:set_driver_status(State#state.driver, working),
				cache:set_driver_online(State#state.driver, self(), NewSt, 0),
				FromPid ! { Ref, ok },
				{NewSt, Rest};
			{resend_order, _Mode, Ref, FromPid, _Order, _Time, _WhenTaken} ->
				FromPid ! { Ref, { error, driver_not_free } },
				{State, Rest};

			{order_cancelled, NewSt, Order} ->
				lager:info("got order_cancelled message from orders process; Driver: ~p  Order: ~p",[(NewSt#state.driver)#driver.id,Order#order.id]),
				gen_tcp:send(Sock,
							 pkt:order_cancelled(State#state.client_protocol, <<170:8, 9:16/little,
													 ((State#state.driver)#driver.id):16/little>>, Order)),
				{NewSt, Rest};
			{tcp, Sock, Bytes} ->
				split_packets(<<Rest/binary,Bytes/binary>>, State#state{ last_tcp = cursecs() }, Sock);
			{ new_socket, NewSock } ->
				gen_tcp:close(Sock),
				{ State#state{ last_tcp = cursecs() }, Rest, NewSock };
			tcp_check ->
				case (cursecs() - State#state.last_tcp) > (client_timeout() div 1000) of
					true ->
						log(State, "tcp client timeouted"),
						worker_finalize(Sock, State, client_timeout);
					false ->
						tcp_check_timer(),
						{State, Rest}
				end;
			pause_tick ->
				St = State#state {
					pause_timer = undefined,
				  pause_total = State#state.pause_total + ?PAUSE_TICK
				},
				NewSt = case State#state.status of
					pause ->
						pause_timer(St);
					_ ->
						St#state {
							pause_begin = undefined
						}
				end,
				{ NewSt, Rest };
			Unknown ->
				log(State, "Received unknown message ~p", [ Unknown ]),
				{ State, Rest }
		after client_timeout() ->
				log(State, "client timeouted"),
				worker_finalize(Sock, State, client_timeout)
		end,
	case Result of
		{NewState, NewRest} ->
			cache:set_driver_online((State#state.driver)#driver.id, self(), NewState, 0),
			?MODULE:tcp_worker_loop(Sock, NewState, NewRest);
		{NewState, NewRest, NewSock0} ->
			cache:set_driver_online((State#state.driver)#driver.id, self(), NewState, 0),
			?MODULE:tcp_worker_loop(NewSock0, NewState, NewRest)
	end.

tcp_check_timer() ->
	erlang:send_after(client_timeout(), self(), tcp_check).

pause_timer(State) ->
	if
		State#state.pause_timer =:= undefined ->
			State#state{
				pause_timer = erlang:send_after(?PAUSE_TICK * 1000, self(), pause_tick)
			};
		true ->
			State
	end.

cleanup_disconnected(State) ->
	log(State, "performing cleanup for disconnected"),
	if is_integer((State#state.driver)#driver.id) ->
			cache:set_driver_online((State#state.driver)#driver.id, disconnected, State, cursecs()),
			cache:set_driver_status((State#state.driver)#driver.id, offline);
		 true ->
			ok
	end.

extreme_cleanup(Pid) ->	
	case ets:match(drivers_online, {'$1', Pid, '$2', '_'} ) of
		[] -> 
			lager:error("performing cleanup extreme; no driver in drivers_online found for Pid ~p",[Pid]),
			no_pid;
		[[DriverId, State]|_] ->
			log(State, "performing cleanup extreme"),
			cache:set_driver_online(DriverId, disconnected, State, cursecs()),
			cache:set_driver_status(DriverId, offline),
			ok				
	end.		

split_packets(<<Bytes/binary>>, State, _Sock) when size(Bytes) < 10 ->
	{State, <<Bytes/binary>>};

split_packets(<<170:8,
				Useless:5/binary,
				LoadSize:16/little,
				Load/binary>>,
				State, _Sock) when size(Load) < LoadSize +2 ->
	{State, <<170:8,
				Useless:5/binary,
				LoadSize:16/little,
				Load/binary>>};

split_packets(<<170:8,
				Useless:5/binary,
				LoadSize:16/little,
				Load:LoadSize/binary,
				170:8,
				170:8,
				Rest/binary>>, 
				State, Sock) ->
	Packet = <<170:8,
				 Useless/binary,
				 LoadSize:16/little,
				 Load/binary,
				 170:8,
				 170:8>>,
	{Response, NewState} = process_packet(Sock, Packet, State),
	cache:set_driver_online((NewState#state.driver)#driver.id, self(), NewState, 0),
	case Response of
		void -> 
			ok;
		_ -> 
			case gen_tcp:send(Sock, Response) of
				{error, Reason} ->
					log(State, "Problem sending packet with reason ~p",[ Reason ]);
				ok -> 
					ok
			end
	end,
	split_packets(Rest, NewState, Sock);

split_packets(<<_:1/binary, Rest/binary>>, State, Sock) ->
	split_packets(Rest, State, Sock).


reauth(Protocol, DriverID, Pass, Head, State, TryAgain)->
	{ Flag, Pkt, NewState } = case cache:get_driver_auth(DriverID, Pass) of
		{ok, Driver, OldState} ->
			NS = if OldState#state.name =:= negative_balance ->
						 OldState#state{driver = Driver, offers = [], name = unreg};
					true ->
						OldState#state{driver = Driver, offers = []}
				 end,
			{ok, pkt:parkings_list(Head, Driver), pause_timer(NS#state{ pause_timer = undefined, client_protocol = Protocol })};
		fail ->
			if TryAgain ->
					reauth(Protocol, DriverID, hex:hashCheapToExpensive(Pass), Head, State, false);
				 true ->
					log(DriverID, "bad auth pair from Driver: ~p pass: ~p", [DriverID, hex:bin_to_hexstr(Pass)]),
					self() ! auth_err,
					{fail, pkt:auth_error(Head, bad_pass), State}
			end;
		{ double_login, DriverId } ->
			{{double_login, DriverId }, pkt:auth_error(Head, double_login), State};
		{negative_balance, Driver, _} ->
			{negative_balance, pkt:parkings_list(Head, Driver),
			 State#state{driver = Driver, name = negative_balance, client_protocol = Protocol}}
	end,
	NewState0 = if NewState#state.client_status =:= pause ->
			pause_timer(if
				NewState#state.pause_begin =:= undefined ->
					NewState#state {
						pause_begin = cursecs()
					};
				true -> NewState
		  end);
		true -> NewState
	end,
	{ Flag, Pkt, NewState0 }.

process_packet(Sock, <<170:8,
				 Department:16/little,
				 DriverID:16/little,
				 Opcode:8,
				 LoadSize:16/little,
				 Load:LoadSize/binary,
				 _Crc:16/little>>,
				 State) ->
	Head = <<170:8, Department:16/little, DriverID:16/little>>,
  log(State, "Receive packet ~p ~p", [ Opcode, Load ]),
	case {Opcode, State#state.name} of 
		{1, start} -> 
			case Load of
				<<MajorVersion:8, MinorVersion, ImeiLen:16/little, Imei:ImeiLen/binary, BPass/binary>> ->					
					log(DriverID, "auth packet version: ~p.~p; IMEI: ~p",[ MajorVersion, MinorVersion, binary_to_list(Imei)]),
					if (MajorVersion =/= ?CLIENT_MAJOR_VERSION) andalso (MajorVersion =/= ?CLIENT_MAJOR_VERSION - 1) ->
							log(State, "bad auth packet from Driver: ~p ; version: ~p.~p; required version: ~p.x",[DriverID, MajorVersion, MinorVersion, ?CLIENT_MAJOR_VERSION]),
							self() ! auth_err,
							{pkt:auth_error(Head, bad_version), State};
						 true ->
							case reauth(MajorVersion, DriverID, hex:hashCheap(BPass), Head, State, both_hashes()) of
								{ { double_login, DriverId }, _, _ } ->
									Pid = cache:get_driver_proc(DriverId),
									send_socket(Pid, Sock),
									worker_finalize(undefined, State, normal);
								{ _Other, Pkt, NewState } -> { Pkt, NewState }
							end
					end;
				_ ->
					log(DriverID, "bad auth packet"),
					self() ! auth_err,
					{pkt:auth_error(Head, bad_badmatch), State}						
			end;

		{5, main} when Load =:= <<0:8>> -> 
			log(State, "unregistration from parking: ~p",[State#state.parking ]),
			cache:do_unregister_driver(State#state.parking, State#state.driver),
			NewState = State#state{name = unreg, parking=0},
			{pkt:parkings_info(Head, NewState), NewState };

		%% {5, main} when Load =/= <<0:8>> ->
		%% 	<<NewParking:8>> = Load,
		%% 	cache:do_reregister_driver(State#state.parking, NewParking, State#state.driver),     
		%% 	NewState = State#state{parking=NewParking},
		%% 	{pkt:parkings_info(Head, NewState), NewState};

		{5, Name} when Name =:= main,  Load =/= <<0:8>>, State#state.orderid =:=0, (State#state.driver)#driver.state =:= work; 
						 Name =:= unreg, Load =/= <<0:8>>, State#state.orderid =:=0, (State#state.driver)#driver.state =:= work  ->
			<<NewParking:8>> = Load,
			log(State, "registration on parking: ~p ; old parking: ~p ",[NewParking, State#state.parking ]),
			cache:do_reregister_driver(State#state.parking, NewParking, State#state.driver),
			NewState = State#state{name = main, parking=NewParking},
			{pkt:parkings_info(Head, NewState), NewState};

		{4, negative_balance} ->
			{pkt:parkings_info(Head, State), State};

		{4, S}  when S =:= main; S =:= unreg ->
			<<NewStatusI:8, %% статус:
				ClientModeI:8, %% режим клиента
				GpsX:32/bitstring, %% координата X
				GpsY:32/bitstring, %% координата Y
				GpsZ:32/bitstring, %% координата Z
				_:64
			>> = Load,
			NewStatus = status2a(NewStatusI),
			NewClientMode = bin_to_client_mode(ClientModeI),
			NewGps = { bin_to_gps(GpsX), bin_to_gps(GpsY), bin_to_gps(GpsZ) },
      log(State, "Receive newGPS ~p", [NewGps]),
			if
				State#state.client_status =:= NewStatus  -> ok;
				true -> cache:set_driver_status(State#state.driver, NewStatus)
			end,

			if
				State#state.client_mode =:= NewClientMode -> ok;
				true -> cache:set_client_mode((State#state.driver)#driver.id, NewClientMode)
			end,

			if
				State#state.gps =:= NewGps -> ok;
				true -> cache:set_gps((State#state.driver)#driver.id, NewGps)
			end,

			%lager:info("got new status ~p; raw status ~p from Driver#~p",[NewStatus, NewStatusI, (State#state.driver)#driver.id]),

			{Response, NewState } = case NewStatus of
				order_finished when State#state.orderid =:= 0 ->
					{pkt:parkings_info(Head, State), State};
				order_finished ->
					{ _, NewSt } = finish_order(State, order_finished),
					{pkt:parkings_info(Head, State), NewSt};
				client_not_came when State#state.orderid =:= 0 ->
					{pkt:parkings_info(Head, State), State};
				client_not_came ->
					{ _, NewSt } = finish_order(State, client_not_came),
					{pkt:parkings_info(Head, State), NewSt};
				missing_client when State#state.orderid =/= 0, State#state.status =/= missing_client ->
					log(State, "got new status ~p;  sending message",[NewStatus ]),
					sql ! {missing_client, {State#state.orderid, (State#state.driver)#driver.id}},
					{pkt:parkings_info(Head, State), State#state{status = NewStatus}};
				missing_client when State#state.orderid =/= 0, State#state.status =:= missing_client ->
					%lager:info("got old status ~p;  from Driver#~p; ignoring",[NewStatus, (State#state.driver)#driver.id]),
					{pkt:parkings_info(Head, State), State};
				_ -> {pkt:parkings_info(Head, State), State#state{status = NewStatus}}
			end,

			NewState0 = case { driver:attr_has_autoappointment(NewState#state.driver), NewState#state.client_status, NewStatus } of
				{ true, Prev, pause } when Prev =/= pause ->
						Time = cursecs() + time_for_pause(State),
		        cache:set_pause(NewState#state.driver#driver.id, Time, undefined),
						pause_timer(if
								NewState#state.pause_begin =:= undefined ->
									NewState#state {
										pause_begin = cursecs()
									};
								true -> NewState
						  end);
				{ true, pause, New } when New =/= pause ->
					cache:delete_pause(NewState#state.driver#driver.id),
					NewState;
				_ -> NewState
			end,
			lager:info("State ~p", [ NewState0 ]),
			{ Response, NewState0#state {
				client_status = NewStatus,
				client_mode = NewClientMode,
				gps = NewGps
			}};

		{6, S} when S =:= main; S =:= unreg ->
			<<ParkingId:8>> = Load,
			log(State, "requested orders list for parking: ~pOrders: ~p",[ ParkingId, cache:get_parkings_orders(ParkingId)]),
			B = case cache:get_parkings_orders(ParkingId) of
						[] -> <<>>;
						Orders -> << <<(pkt:order_offer(State#state.client_protocol, Head, Order, true))/binary>> ||Order <- Orders>>
					end,
			{<<B/binary,(pkt:finitor(Head))/binary>>, State};

		{7, S } when Load =:= <<0:40>>, (S =:= main orelse S =:= unreg) ->
			log(State, "refuses orders: ~p", [ State#state.offers]),
			lists:map(fun({OrderId,Token})->
				case cache:get_order(OrderId) of
					{Pid, _} ->
						Pid ! {answer, refuse, self(), Token},
						ok;
					_ ->
						log(State, "cant find order to refuse from or refuse is too slow; Order: ~p", [ OrderId ]),
						fail
				end
			end, State#state.offers),
			cache:do_reregister_driver(State#state.parking, State#state.parking, State#state.driver),
			{void, State#state{offers = []}};

		{7, S } when Load =/= <<0:40>>, State#state.orderid =:= 0, (State#state.driver)#driver.state=:=work, (S =:= main orelse S =:= unreg) ->
			<<DriverResponse:8, OrderId:32/big>> = Load,
			LegalOrder = lists:keymember(OrderId, 1, State#state.offers),
			{Pid, Order} = cache:get_order(OrderId),
			case (cursecs() - row_timeout(State#state.driver)) of
				Q when Q > State#state.lasttake, LegalOrder ->
					A = code2time(DriverResponse),
					When = calendar:datetime_to_gregorian_seconds(erlang:universaltime())-calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}}),
					{OrderId, Token} = lists:keyfind(OrderId, 1, State#state.offers),
					Pid ! {answer, A, When, (State#state.driver)#driver.id, Token},

					receive
						{acc_offer, O} when O#order.id =:= OrderId ->
							Pid ! acc_offer_ok,

							cache:do_unregister_driver(State#state.parking, State#state.driver),
							cache:set_driver_status((State#state.driver)#driver.id, working),
							NewState = State#state{name = unreg, orderid = OrderId, status = working, parking = 0, offers= [], lasttake = cursecs()},
							%%{void, NewState};
							%%{pkt:order_not_confirmed(Head, Order), State#state{fails = State#state.fails+1}};
							{pkt:order_confirm(State#state.client_protocol, Head, Order, A, When), NewState};
						{acc_offer, O} when O#order.id =/= OrderId ->
							log(State, "wrong order accept confirmation from order process; should_Order: ~p got_Order: ~p",[ OrderId, O#order.id]),
							{pkt:order_not_confirmed(State#state.client_protocol, Head, Order), State#state{fails = State#state.fails+1}}
					after 10000 ->
						log(State, "can't wait for order accept confirmation from order process; Driver: ~p Order: ~p",[(State#state.driver)#driver.id ,OrderId]),
						{pkt:order_not_confirmed(State#state.client_protocol,Head, Order), State#state{fails = State#state.fails+1}}
					end;

				_ ->
					log(State, "attempt to take second order too quick or take illegal order"),
					{_Pid, Order} = cache:get_order(OrderId),
					{pkt:order_not_confirmed(State#state.client_protocol, Head, Order), State#state{fails = State#state.fails+1}}
			%%{void, State#state{fails = State#state.fails+1}}
			end;

		{8,_} ->
			{pkt:balance(Head, cache:get_new_balance((State#state.driver)#driver.id)), State};

		{9, _} ->
			if is_record(State#state.lastMessage, message) ->
				log(State, "got message confirmation, Message: ~p",[ (State#state.lastMessage)#message.id]),
				case State#state.lastMessage of
					X when is_record(X, message) ->
						cache:update_message(State#state.lastMessage);
					_ ->
						whatever
				end;
				true ->
					whatever
			end,
			{void, State#state{lastMessage = no_message}};
		{ 10, _ } when State#state.orderid > 0 ->
			<<  _TarifType:8/unsigned-integer,
				  OrderId:32/big-unsigned-integer,
				  Payout:16/big-unsigned-integer,
			    DistanceTotal:16/big-unsigned-integer,
			    CashDistance:16/big-unsigned-integer,
			    TimeTotal:16/big-unsigned-integer,
			    CashTime:16/big-unsigned-integer,
			    CashPauseTime:16/big-unsigned-integer,
			    WaitTimeTotal:16/big-unsigned-integer,
			    CashWaitTime:16/big-unsigned-integer,
			    CashBaggage:8/big-unsigned-integer
			>> = Load,
			if
				State#state.orderid =/= OrderId ->
					{ pkt:fail(Head, missing_orderid , "Bad order_finished packet. Received order ID = ~p but State.orderid = ~p", [ OrderId, State#state.orderid ] ), State};
				true ->
					sql:order_cash_save(State#state.orderid, Payout, DistanceTotal, CashDistance, TimeTotal, CashTime, CashPauseTime, WaitTimeTotal, CashWaitTime, CashBaggage ),
					{ pkt:ok(Head), State }
			end;
		{ 10, _ } ->
			{ pkt:fail(Head, bad_driver_status, log(State, "Bad packet #10 because driver is free ")), State };
		{ 11, _ } ->
			<< _:8 >> = Load,
			{ pkt:pause_time(Head,
					time_for_pause(State),
					State#state.pause_begin
			), State };
		{ 14, _ } ->
			{ pkt:free_orders(Head), State };
		Q ->
			log(State, "bad opcode,state pair: ~p ",[ Q ]),
			{void, State#state{fails = State#state.fails+1}}
	end;

process_packet(_Sock, Packet, State) ->
	log(State, "failed packet: ~p~nState: ~p",[ Packet, State ]),
	{void, State#state{fails = State#state.fails+1}}.

log(#state{} = State, Msg, Param) ->
	log(State#state.driver, Msg, Param);

log(Driver, Msg, Param) ->
	driver:log(Driver, Msg, Param).

log(Driver, Msg) ->
	log(Driver, Msg, []).

-spec finish_order(#state{}, atom()) -> { ok|fast|order_not_found, #state{} }.
finish_order(State, NewStatus) ->
	FastFinish = (cursecs() - State#state.lasttake) < finish_timeout(),
	if
		FastFinish ->
			log(State, "attempt to finish Order: ~p too fast ", [State#state.orderid ]),
			{ fast, State };
		true ->
			NState = if
				(State#state.driver)#driver.state =:= cant_work ->
					State#state{status = NewStatus, orderid = 0, name = unreg, parking = 0};
				true ->
					State#state{status = NewStatus, orderid = 0}
			end,
			case cache:get_order(State#state.orderid) of
				{Pid, _} ->
					Pid ! { finished, NewStatus },
					{ ok, NState#state { lastfinish = cursecs() } };
				no_order ->
					log(State,"cant find Order: ~p just 'finished'", [State#state.orderid ]),
					{ order_not_found, NState }
			end
	end.


time_for_pause(State) ->
		State#state.driver#driver.pause_total - State#state.pause_total.


send_message(Pid, Message) when is_record(Message, message) ->
	Pid ! Message.

%% --------------- CANAL API ----------------

refresh_info(Pid, DriverNew ) ->
	Pid ! { new_auth, DriverNew }.

send_socket(Pid, Socket) when is_pid(Pid) ->
	gen_tcp:controlling_process(Socket, Pid),
	Pid ! { new_socket, Socket }.

