-module(config).
-author("denilen").

-include("logger.hrl").

%% API
-export([start_link/1, get/1, get/2, update/0]).

-behaviour(gen_server).
-export([
	init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
  terminate/2,
  code_change/3
]).

-record(state, {
	filename :: string()
}).

-define(ETS, ?MODULE).
-define(SERVER, ?MODULE).

update() ->
	gen_server:call(?SERVER, update).

start_link(Filename) ->
	{ ok, Pid } = gen_server:start_link({local, ?MODULE}, ?MODULE, { Filename }, []),
	ok = update(),
	{ ok, Pid }.

init({Filename}) ->
  ets:new(?ETS, [ named_table, set, protected, {read_concurrency, true} ]),
	{ ok, #state{
		filename = Filename
	}}.

get(Key, Default) ->
  case ets:lookup(?ETS, Key) of
		[] -> Default;
		[ { _, Value } ] -> Value
  end.

get(Key) ->
  case ets:lookup(?ETS, Key) of
		[] -> error({no_config_key, Key });
		[ { _, Value } ] -> Value
  end.

%% ------- gen_server API ----------------

handle_call(update, _, State) ->
	{ok, Config } = file:consult(State#state.filename),
	ets:delete_all_objects(?ETS),
  ets:insert(?ETS, Config),
	{ reply, ok, State };

handle_call(_, _, State) ->
	{ reply, error, State }.

handle_cast(_, State) ->
	{ noreply, State }.

handle_info(_, State) ->
	{ noreply, State }.

terminate(_, _State) -> ok.

code_change(_, State, _) -> { ok, State }.
