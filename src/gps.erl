-module(gps).
-author("denilen").

-include("logger.hrl").

%% API
-export([distance/2, in_radius/3, from_scale/1, to_scale/1, lon/1, lat/1, is_gps/1, is_gps_list/1]).

-define(SCALE, 1000000).

%% Является ли расстояние между точками меньше чем radius (км)
in_radius(Radius, Gps1 , Gps2) ->
  distance(Gps1, Gps2) =< Radius.

distance(Gps1, Gps2) ->
  NewGps1 = to_radian(from_scale(Gps1)),
  NewGps2 = to_radian(from_scale(Gps2)),
  distance(lon(NewGps1), lat(NewGps1), lon(NewGps2), lat(NewGps2)).

lon({X,_Y,_Z}) ->
  X;
lon(_) -> undefined.

lat({_X, Y, _Z}) ->
  Y;
lat(_) -> undefined.

%% Дистанция между точками в метрах
distance(undefined,_,_,_) -> undefined;
distance(_,undefined,_,_) -> undefined;
distance(_,_,undefined,_) -> undefined;
distance(_,_,_,undefined) -> undefined;
distance(0,0,_,_) -> undefined;
distance(_,_,0,0) -> undefined;
distance(Lon1, Lat1, Lon2, Lat2) ->
  %% Формула гаверсинусов
  6372795 * 2 * math:asin(math:sqrt(pow2((Lat1-Lat2)/2)+math:cos(Lon1)*math:cos(Lon2)*pow2(math:sin((Lon1-Lon2)/2)))).

to_scale({X,Y,Z}) ->
  { to_scale(X), to_scale(Y), Z };
to_scale(Angle) when is_float(Angle)->
  round(Angle * ?SCALE);
to_scale(_) -> undefined.

from_scale({X,Y,Z}) ->
  { from_scale(X), from_scale(Y), Z };
from_scale(Angle) when is_integer(Angle) ->
  Angle / ?SCALE;
from_scale(_) -> undefined.

to_radian({0,0,_}) -> undefined;
to_radian({X,Y,Z}) ->
  { to_radian(X), to_radian(Y), Z };
to_radian(0) -> 0;
to_radian(X) when is_float(X) ->
  X / 180 * math:pi();
to_radian(_) -> undefined.

is_gps({0,0,_}) -> false;
is_gps({X,Y,_Z}) ->
  is_integer(X) andalso is_integer(Y);
is_gps(_) -> false.

is_gps_list(Gps) when is_list(Gps) ->
  lists:all(fun(X) -> is_gps(X) end, Gps).

pow2(X) ->
  X * X.
