-module(orders).
-author("denilen").

-compile(export_all).

-include("logger.hrl").

-include_lib("./common.hrl").
-include_lib("./config.hrl").
-include_lib("./rules.hrl").

-define(AUTOAPPOINTMENT_RADIUS, config:get(autoappointment_radius)).	%% Радиус зоны автозаказа, метров
-define(WAIT_TIME, 60000000).						%% Время ожидания (секунд) назначения водителя, после которого заказ будет отправлен другому серверу.

-record(order_state, {
	order :: #order{},
	refusals = [] :: [ {DriverId :: integer(), Secs :: integer()} ],
	order_start = undefined :: integer()
}).

init(Order) ->
	cache:do_register_order(Order),
	start(#order_state{
		order = Order,
		refusals = [],
		order_start = cursecs()
	}).

filter_refusals(_, []) -> [];
filter_refusals(DriversQueueCount, Ref) ->
	Timeout = cursecs() - max(refusal_timeout(), 30 + (DriversQueueCount * offer_timeout() div  1000)),
	[{Id, Secs} || {Id, Secs} <- Ref, Secs > Timeout ].

filterout_refusals([], _) -> [];
filterout_refusals(Drivers, []) -> Drivers;
filterout_refusals(Drivers, Refusals) ->
	[Driver || Driver <- Drivers,
				 not(lists:any(fun({Id, _}) ->
									 Driver#driver.id =:= Id 
							 end, Refusals))].

drivers_queue_econom(All) ->
	NotEconom = [D || D <- All,
						not(driver:has_attr(?DRIVER_ATTR_ECONOM, D))],
	case NotEconom of 
		[] ->
			All;
		NE ->
			NE
	end.

drivers_queue(Order) ->
	Parking = Order#order.parking,
	All = driver:only_free_online(cache:get_parkings_drivers(Parking) ++ cache:get_drivers_by_attr(?DRIVER_ATTR_AUTOAPPOINTMENT)),
	case econom_logic() of
		true ->
			drivers_queue_econom(All);
		false ->
			All
	end.


start(#order_state{} = State) ->
	?MODULE:start_loop(State).

start_loop(#order_state{} = State) ->
	Order = State#order_state.order,
	case ((cursecs() - State#order_state.order_start) > ?WAIT_TIME)  of
		true -> forget_order(State);
		false ->
			receive
				cancelled ->
					cache:do_unregister_order(Order),
					cache:set_unusual_order_state(Order#order.id),
					cache:do_remove_order(Order#order.id);
				_ ->
					start(State)
			after 0 ->
				NewOrder = case cache:get_new_order(Order#order.id) of
				 Order ->
					 %%unchanged,
					 Order;
				 Q when erlang:is_record(Q, order) ->
					 lager:info("update Order: ~p~n",[Order#order.id]),
					 Q;
				 _ ->
					 lager:error("order cant find self in orders table, proceeding without update; Order: ~p~n", [Order]),
					 Order
				end,
				DriversQueue = drivers_queue(NewOrder),
				Refusals = filter_refusals(length(DriversQueue), State#order_state.refusals),
				NewState = State#order_state{
					order = NewOrder,
					refusals = Refusals
				},
				case select_driver(NewOrder, cache:get_rule(NewOrder#order.rule_head), filterout_refusals(DriversQueue, Refusals)) of
					no_driver ->
						wait_for_driver(NewState);
					{ Driver, Attr } ->
						lager:info("Order ~p to driver (~p) ~p", [ Order#order.id, Attr, Driver ]),
						case Attr of
							?DRIVER_ATTR_AUTOAPPOINTMENT ->
								appoint_order(NewState, Driver);
							_ ->
								try_offer(NewState, Driver)
						end
				end
			end
	end,
	ok.

wait_for_driver(State) ->
	timer:sleep(no_drivers_timeout()),
	start(State).

-spec distance_filter([{Distance :: float(), Obj :: any()} ], float()) -> [Obj :: any()].
distance_filter(DistanceLimit, DistanceList) ->
	[ Obj || { Distance, Obj } <- lists:keysort(1, DistanceList), is_number(Distance), Distance =< DistanceLimit ].

select_driver(_Order, _RuleSetList, []) -> no_driver;
select_driver(_Order, all, _Drivers) -> no_driver;
select_driver(_Order, [], _Drivers) -> no_driver;
select_driver(Order, [RuleSet|OtherRules] , Drivers) ->
	%%lager:info("Select drivers for order ~p ~n RuleSet ~p ~n Drivers ~p ", [ Order#order.id, RuleSet, [ Driver#driver.id || Driver <- Drivers] ]),
	case filter_drivers_by_ruleset(RuleSet, Drivers) of
		[] ->
			select_driver(Order, OtherRules, Drivers);
		Drivers0 ->
			[ Rule | _ ] = sets:to_list(RuleSet),
			Attr = driver:get_rule_attr(Rule),
			case process_rule(Attr, Order, Drivers0) of
				[] -> select_driver(Order, OtherRules, Drivers);
				[Driver|_] -> { Driver, Attr }
			end
	end.

filter_drivers_by_ruleset(RuleSet, Drivers) ->
	[Driver || Driver <- Drivers, sets:is_element(Driver#driver.attributes, RuleSet)].

process_rule(_, _, []) -> [];
process_rule(?DRIVER_ATTR_AUTOAPPOINTMENT, Order, Drivers) ->
		Drivers0 = [ { Driver, driver:gps(Driver) } || Driver <- Drivers ],
		Drivers1 = distance_filter(?AUTOAPPOINTMENT_RADIUS, [ { gps:distance(Order#order.gps_from, Gps), { Driver, Gps }} || { Driver, Gps } <- Drivers0, gps:is_gps(Gps) ]),
		distance_filter(?AUTOAPPOINTMENT_RADIUS, [ { osrm:distance([Gps, Order#order.gps_from]), Driver } || { Driver, Gps } <- Drivers1 ]);

process_rule(_, _, Drivers) -> Drivers.

-spec add_refusals(#order_state{}, #driver{}) -> #order_state{}.
add_refusals(State, Driver) ->
	State#order_state{
		refusals = [{Driver#driver.id, cursecs()}| State#order_state.refusals]
	}.

try_offer(State, Driver) ->
	Order = State#order_state.order,
	lager:info("trying to offer Order: ~p to Driver: ~p~n",[Order#order.id, Driver#driver.id]),
	case do_offer(Order, Driver) of 
		timeout -> 
			driver:log( Driver, "refuse by timeout. Order: ~p",[Order#order.id]),
			case cache:get_driver_proc(Driver#driver.id) of
				Pid when is_pid(Pid) ->
					Pid ! {remove_offer, Order};
					_ -> whatever
				end,
			%%cache:do_reregister_driver(Order#order.parking, Order#order.parking, Driver),
			start(add_refusals(State, Driver));
		refuse -> start(add_refusals(State, Driver));
		{taken, Time, When} -> 
			case cache:get_driver_proc(Driver#driver.id) of
				Pid when is_pid(Pid) ->
					Pid ! {acc_offer, Order},

					receive 
						acc_offer_ok ->
							lager:info("Order: ~p taken by Driver: ~p", [Order#order.id, Driver#driver.id]),
							cache:do_unregister_order(Order),
							cache:set_order_state(Order, Driver, Time, continue),
							erlang:send_after(execution_timeout(), self(), execution_timeout),
							taken(Order, Driver, Time, When)
					after offer_timeout() ->
							lager:warning("can't wait for confirmation of reception of order acception confirmation from client process; Driver: ~p Order: ~p",[Driver#driver.id, Order#order.id]),
							start(add_refusals(State, Driver))
					end;
				_ ->
					start(add_refusals(State, Driver))
			end
	end.

%% Принудительное назначение заказа водителю
appoint_order(State, Driver) ->
	Order = State#order_state.order,
	Time = 15,
	When = calendar:datetime_to_gregorian_seconds(erlang:universaltime())-calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}}),
	case cache:get_driver_proc(Driver#driver.id) of
		Pid when is_pid(Pid) ->
			case canal:resend_order(Pid, Order, Time, When, auto) of
				ok ->
					driver:log(Driver,"taken order: ~p", [Order#order.id]),
					cache:do_unregister_order(Order),
					cache:set_order_state(Order, Driver, Time, continue),
					erlang:send_after(execution_timeout(), self(), execution_timeout),
					taken(Order, Driver, Time, When);
				{ error, _ } ->
					start(add_refusals(State, Driver))
			end;
		_ ->
			start(add_refusals(State, Driver))
	end.

%this clause is merely a reminiscent of a bug conserning broken rules
do_offer(Order, Driver) when not(is_record(Driver, driver)) ->
	lager:warning("Bug: Driver is not a record, refusing. Driver: ~p; Order: ~p",[Driver, Order]),
	refuse;

do_offer(Order, Driver) when is_record(Driver, driver) ->
	case cache:get_driver_proc(Driver#driver.id) of
		Pid when is_pid(Pid) ->
			Token = make_ref(),
			Pid ! {offer, Order, Token},
			DrId =  Driver#driver.id,
			receive
				{answer, refuse, _, Token} -> refuse;
				{answer, Time, When, DrId, Token} -> {taken, Time, When}
			after 
				offer_timeout() -> timeout
			end;
		_ -> 
			lager:error("trying to find driver process failed Driver: ~p Order: ~p",[Driver#driver.id,Order#order.id]),
			refuse
	end.

taken(Order, Driver, Time, When) ->
	receive
		{ finished, NewStatus } ->
			driver:log(Driver, "order: ~p finished with status ~p",[Order#order.id, NewStatus]),
			cache:set_order_state(Order, Driver, Time, NewStatus),
			cache:do_remove_order(Order#order.id);
		cancelled ->
			lager:info("Order: ~p cancelled after take by Driver: ~p",[Order#order.id, Driver#driver.id]),
			case ets:lookup(drivers_online, Driver#driver.id) of 
				[{_, Pid, State, OfflineSince}] ->
					NewState =  State#state{orderid=0, lasttake=0, parking=0, name=unreg, status=pause},
					case Pid of
						Pid when is_pid(Pid) ->
							cache:set_driver_status((State#state.driver)#driver.id, pause),
							cache:set_driver_online((State#state.driver)#driver.id, Pid, NewState, 0),
							Pid ! {order_cancelled, NewState, Order};
						Pid when Pid=:=disconnected -> 
							cache:set_driver_online((State#state.driver)#driver.id, Pid, NewState, OfflineSince)
					end;
				_ ->
					lager:info("cancelled Order: ~p cannot find Driver: ~p to inform; ololo",[Order#order.id, Driver#driver.id])
			end,
			cache:do_unregister_order(Order),
			cache:set_unusual_order_state(Order#order.id),
			cache:do_remove_order(Order#order.id);
		{driver_reconnect, NewPid} ->
			canal:resend_order(NewPid, Order, Time, When, forced),
			taken(Order, Driver, Time, When);
		execution_timeout ->
			lager:error("Order: ~p isn't finished in ~p ms; order process killed",[Order#order.id, execution_timeout()]),
			cache:set_order_state(Order, Driver, Time, execution_timeout),
			self() ! cancelled,
			taken(Order, Driver, Time, When)
			%do_something
	end.

forget_order(#order_state{} = State) ->
	Order = State#order_state.order,
	cache:do_unregister_order(Order),
	cache:set_forget_order_state(Order#order.id),
	cache:do_remove_order(Order#order.id),
	lager:info("Order ~p was forgotten", [ Order#order.id ]).



