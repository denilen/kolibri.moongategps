-module(cache).
-author("denilen").

-compile(export_all).

-export([it_is_driver_proc/1]).

-include("logger.hrl").
-include_lib("./common.hrl").
-include_lib("./config.hrl").

start(PPid) ->
	erlang:register(cache, self()),
	Pid = spawn_link(sql, start, [self()]),
	receive
		start_ok -> ok;
		{'EXIT', Pid, Why} -> 
			lager:error("sql start fail: ~p~n", [Why]),
			io:format("sql start fail: ~p~n", [Why]),
			erlang:exit({sql_start_fail, Why})
	end,
	
	prepare(PPid).
	
prepare(PPid) ->
	ets:new(drivers_auth, [set, named_table, public, {keypos, 2}]),
	
	receive
		{auth, List} ->
			ets:insert(drivers_auth, List)
	end,

	ets:new(drivers_online, [set, named_table, public]),
	timer:send_interval(flush_disconnected_period(), cache, flush_disconnected),

	ZonesKeypos = case sort_zones_by_id() of
					  true -> 1;
					  false -> 2
				  end,
	ets:new(parkings, [ordered_set, named_table, public, {keypos, ZonesKeypos}]),

	ets:new(drivers_statuses, [set, named_table, public]),
	timer:send_interval(drivers_status_timeout(), cache, flush_statuses),

	ets:new(drivers_client_mode, [set, named_table, public]),
	ets:new(drivers_gps, [set, named_table, public]),

	ets:new(drivers_distrib, [set, named_table, protected]),
	receive
		{parkings, List1} ->
			ets:insert(parkings, List1)
	end,
	ets:insert(drivers_distrib, [{Id, [], []} 
								 || {Id, _} <- List1]),
	timer:send_interval(distrib_timeout(), cache, flush_distrib),

	ets:new(rules, [set, named_table, protected]),
	receive
		{queue_rules, RList} ->
			ets:insert(rules, RList)
	end,

	ets:new(orders, [set, named_table, public]),


	ets:new(orders_states, [bag, named_table, public]),
	timer:send_interval(orders_states_timeout(), cache, flush_orders_states),

	ets:new(messages, [set, named_table, public, {keypos, 2}]),
	ets:new(announces, [set, named_table, public, {keypos, 2}]),
	timer:send_interval(messages_cleanup_timeout(), cache, cleanup_announces),

	receive 
		{messages, List2} ->
			receive_messages(List2)			
	end,

	ets:new(drivers_pause, [ set, named_table, public ]),
	timer:send_interval(5000, cache, check_drivers_pause),

	receive 
		{restart_hack, Res} ->
      lists:foreach(fun({Driver, Order, Time2meet}) ->
  		  ets:insert(drivers_online, {Driver#driver.id, disconnected,
          #state{
            driver = Driver,
            status = working,
            name = unreg, %%start, main , unreg, negative_balance
            parking = 0,
            fails = 0,
            orderid = Order#order.id,
            offers = [],
            lasttake = 0,
            lastMessage = no_message,
            client_mode = free_order,
            gps = { 0,0,0 }
          }, cursecs()}),
			  ets:insert(orders, {Order#order.id, starting, Order}),
        OPid = spawn(orders, taken, [Order, Driver, Time2meet*5, cursecs()]),
        ets:update_element(orders, Order#order.id, {2, OPid})   %UNSAFE_RECORD
      end, Res)
	end,
		
	PPid ! start_ok,

	?MODULE:loop().

loop() ->
	receive
		{auth, NList} ->
			OList = ets:tab2list(drivers_auth),
			ets:delete_all_objects(drivers_auth),
			ets:insert(drivers_auth, NList),

			Nip = gb_sets:from_list([{D#driver.id, D#driver.password} || D <- NList]),
			Oip = gb_sets:from_list([{D#driver.id, D#driver.password} || D <- OList]),
			Online = [{Id, Pid} || [Id, Pid] <- ets:match(drivers_online, {'$1','$2','_',0})],
			OnlineIds = ordsets:from_list([Id || {Id, _Pid} <- Online]),
			Oip_and_online = gb_sets:filter(fun({Id, _Pass})->
													ordsets:is_element(Id, OnlineIds)
											end, Oip),
			lists:map(fun ({Id, _Pass})->
							  case lists:keyfind(Id,1,Online) of
								  {Id, Pid} ->
									  Pid ! disconnect;
								  _ ->
									  whatever
							  end
					  end, gb_sets:to_list(gb_sets:difference(Oip_and_online, Nip))),

			Oset = gb_sets:from_list(OList),
			Nset = gb_sets:from_list(NList),
			Nset_and_online = gb_sets:filter(fun(D)->
													 ordsets:is_element(D#driver.id, OnlineIds)
											 end, Nset),
			lists:map(fun (D)->
							  case lists:keyfind(D#driver.id,1,Online) of
								  {_Id, Pid} ->
									  canal:refresh_info(Pid, D);
								  _ ->
									  whatever
							  end
					  end, gb_sets:to_list(gb_sets:difference(Nset_and_online, Oset))),			

			DisconnIds = ordsets:from_list([Id || [Id] <- ets:match(drivers_online, {'$1',disconnected,'_','_'})]),
			Nis = gb_sets:from_list([{D#driver.id, D#driver.state} || D <- NList]),
			Ois = gb_sets:from_list([{D#driver.id, D#driver.state} || D <- OList]),
			Ois_and_disconn = gb_sets:filter(fun ({Id, _St})->
													 ordsets:is_element(Id, DisconnIds)
											 end, Ois),
			Parkings = [ParkingId || {ParkingId, _} <- ets:tab2list(parkings)],
			lists:map(fun ({Id, _St}) ->
							  case ets:match(drivers_online, {Id,disconnected,'$1','_'}) of
								  [[State]] ->
									  unregister_driver_search(Parkings, State#state.driver),
									  ets:delete(drivers_online, Id);
								  _ ->
									  whatever
							  end
					  end, gb_sets:to_list(gb_sets:difference(Ois_and_disconn, Nis)));

		{parkings, List} ->
			OldIds = [ Id || {Id, _} <- ets:tab2list(parkings)],
			Ids = [ Id || {Id, _} <- List],
			NewIds = lists:subtract(Ids, OldIds),
			ets:delete_all_objects(parkings),
			ets:insert(drivers_distrib, [{Id, [], []} || Id <- NewIds]),
			ets:insert(parkings, List);

		{orders_new, List} ->
			%%lager:info("inserting new orders:~p~n",[List]),
			lists:map(fun (Order) ->
							  lager:info("got new Order: ~p~n",[Order#order.id]),
							  ets:insert(orders, {Order#order.id, starting, Order}),
							  OPid = spawn(orders, init,[Order]),
							  ets:update_element(orders, Order#order.id, {2, OPid})   %UNSAFE_RECORD
					  end, List);
		{orders_old, List} ->
			%%lager:info("inserting old orders: ~p~n",[List]),
			lists:map(fun(O) ->
							  case ets:lookup(orders, O#order.id) of
								  [{_, Pid, _}] ->  
									  if O#order.state =:= 3 ->
											  lager:info("cancelled Order: ~p~n",[O#order.id]),
											  %%order finished/cancelled
											  Pid ! cancelled;
										 true ->
											  ets:insert(orders, {O#order.id, Pid, O})
									  end;									  
								  _ -> whatever
							  end
					  end, List);
		{queue_rules, List} ->
			ets:insert(rules, List);
		flush_statuses ->
			sql ! {flush_statuses, { ets:tab2list(drivers_statuses), ets:tab2list(drivers_client_mode), ets:tab2list(drivers_gps) }},
			ets:delete_all_objects(drivers_statuses),
      ets:delete_all_objects(drivers_client_mode),
      ets:delete_all_objects(drivers_gps);
		flush_distrib ->
			OnlineDrivers = [Id || {Id, Pid, _, _} <- ets:tab2list(drivers_online), erlang:is_pid(Pid)],
			F = fun (Drvrs) ->
						[Drvr || Drvr <- Drvrs,
								 lists:member(Drvr#driver.id,OnlineDrivers)]
						end,
			sql ! {flush_distrib, [{Parking, F(Drivers)} || {Parking, _, Drivers} <- ets:tab2list(drivers_distrib)]};
		flush_orders_states ->
			sql ! {flush_orders_states, ets:tab2list(orders_states)},
			ets:delete_all_objects(orders_states);
		{status, DriverId, Status} ->
			ets:insert(drivers_statuses, {DriverId, Status});
		{client_mode, DriverId, ClientMode} ->
			ets:insert(drivers_client_mode, {DriverId, ClientMode});
		{gps, DriverId, Gps} ->
			ets:insert(drivers_gps, {DriverId, Gps});
		{register, ParkingId, Driver} ->
			register_driver(ParkingId, Driver);
		{unregister, ParkingId, Driver} ->
			unregister_driver(ParkingId, Driver);		
		{reregister, OldParking, NewParking, Driver} ->	
			reregister_driver(OldParking, NewParking, Driver);
		{register_order, Order} ->
			register_order(Order);
		{unregister_order, Order} ->
			unregister_order(Order);
		{get_distrib, Pid} -> 
			Pid ! {distrib, [{ParkingId, length(Orders), Drivers} || {ParkingId, Orders, Drivers} <- ets:tab2list(drivers_distrib)]};
		cleanup_announces ->
			lager:info("cleanup announces~n"),

			As = ets:tab2list(announces),
			Time = time2usecs(erlang:localtime()),
			lists:map(fun(Ann) ->
							  sql ! {update_announce, {Ann#message.id, Ann#message.count, Ann#message.dReceive}},
							  B = time2usecs(str2time(Ann#message.dEnd)) < Time,
							  if B ->
									  ets:delete_object(announces, Ann);
								 true ->
									  ok
							  end
					  end, As);
		
		{messages, MList} ->			
			receive_messages(MList);			

		flush_disconnected ->
			lists:map(fun({Id, State}) ->							  
							  unregister_driver_search(
								[ParkingId || {ParkingId, _} <- ets:tab2list(parkings)]
								, State#state.driver),
							  ets:delete(drivers_online, Id),
							  ets:insert(drivers_statuses, {Id, offline})								  
					  end, [{Id, State} || {Id, Pid, State, When} <- ets:tab2list(drivers_online), 
										   Pid =:= disconnected, 
										   (cursecs() - When)> hanging_disconnect_timeout()]);
    check_drivers_pause ->
	    WarnTime = cursecs() + pause_warning_time() * 60,
	    Drivers = ets:tab2list(drivers_pause),
      lists:foreach(fun
	      ({DriverId, PauseEnd, undefined}) when PauseEnd < WarnTime ->
		      Text = io_lib:format("Оставшееся суммарное время отдыха меньше ~p минут. Поторопитесь выйти в смену.", [ pause_warning_time() ]),
	        case get_driver_proc(DriverId) of
						no_driver ->
							case driver:phone(DriverId) of
								undefined -> ok;
								Phone when is_list(Phone) ->
									sms:send(Phone, Text)
							end;
						Pid ->
							canal:send_message(Pid, #message{
								type = message,
								text = Text
							})
					end,
		      set_pause(DriverId, PauseEnd, warning_send);
	      (_) -> ok
		  end, Drivers),
	    Curtime = cursecs(),
	    lists:foreach(fun
		    ({DriverId, PauseEnd, _}) when PauseEnd < Curtime ->
			    ets:delete(drivers_pause, DriverId);
		    (_) -> ok
	    end, Drivers)
	end,	
	?MODULE:loop().

register_driver(ParkingId, Driver) ->
	case ets:lookup(drivers_distrib, ParkingId) of		
		[] -> no_parking;
		[{_, Orders, Drivers}] -> ets:insert(drivers_distrib, {ParkingId, Orders, Drivers++[Driver]})
	end.

unregister_driver_search([], _Driver) ->
	ok;

unregister_driver_search([ParkingId|Rest], Driver) ->
	unregister_driver(ParkingId, Driver),	
	unregister_driver_search(Rest, Driver).

unregister_driver(ParkingId, Driver) ->
	if ParkingId =:= 0 -> no_parking;
	   true ->
			case ets:lookup(drivers_distrib, ParkingId) of
				[] -> no_parking;
				[{_, Orders, Drivers}] -> 
					ets:insert(drivers_distrib, 
						   {ParkingId, Orders, 
						    [D||D <- Drivers, D#driver.id =/= Driver#driver.id]}),
					ok
			end
	end.

reregister_driver(OldParking, NewParking, Driver) ->
	case unregister_driver(OldParking, Driver) of
		no_parking ->
			%lager:info("unregister_driver: failed to find Driver#~p on Parking#~p ; forcing full search~n",[Driver#driver.id, OldParking]),
			unregister_driver_search(
			  [ParkingId || {ParkingId, _} <- ets:tab2list(parkings)], 
			  Driver);
		_ -> ok
	end,
	register_driver(NewParking, Driver).

register_order(Order) ->
	case ets:lookup(drivers_distrib, Order#order.parking) of
		[] -> no_parking;
		[{_, Orders, Drivers}] -> ets:insert(drivers_distrib, {Order#order.parking, [Order|Orders], Drivers})
	end.
	
unregister_order(Order) ->
case ets:lookup(drivers_distrib, Order#order.parking) of
		[] -> no_parking;
		[{_, Orders, Drivers}] -> ets:insert(drivers_distrib, {Order#order.parking, [O || O <- Orders, O#order.id =/= Order#order.id], Drivers})
	end.

receive_messages(MList) ->
	%%lager:info("got new messages: ~p~n",[List]),
	lists:map(fun(Mess) ->
					  if Mess#message.type =:= announce ->
							  ets:insert(announces, Mess),
							  false;
						 true ->
							  if Mess#message.type =:= leader -> 
									  do_nothing;
								 true -> ets:insert(messages, Mess)
							  end,
							  case ets:lookup(drivers_online, Mess#message.carid) of
								  [{_, P, _, _}] when is_pid(P) -> 									  
									  P ! Mess,
									  true;
								  _ ->
									  ok
							  end
					  end
			  end, MList).
	
%%Export----------------------------------------------------------------------------------------------------
set_driver_online(Id, Pid, State, When) ->
	if is_integer(Id) andalso is_integer((State#state.driver)#driver.id) ->
			ets:insert(drivers_online, {Id, Pid, State, When}),
      case Pid == self() of %% пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ :)
        true ->
          cache:it_is_driver_proc(Id);
        false -> ok
      end;
	   true -> 
			ok
	end.

check_for_new_messages(Id) ->
	[self() ! Mess || Mess <- ets:match_object(messages, #message{carid = Id, _ = '_'}), 
					  Mess#message.count =:= 0],
	[self() ! Mess || Mess <- ets:tab2list(announces)].

get_driver_auth(Id, Pass) ->
	case ets:lookup(drivers_online, Id) of
		[{Id, P, _, _}] when is_pid(P) -> 
			driver:log(Id, "double login"),
			{ double_login, Id };
		[{Id, disconnected, State, _}] ->
			case ets:match_object(drivers_auth, #driver{id = Id, password = Pass, _='_'}) of
				[] ->
					lager:info("login failed: wrong password; Driver: ~p~n",[Id]),
					fail;
				[Driver] ->		
					set_driver_status(Id, State#state.status),
					check_for_new_messages(Id),
					if Driver =/= State#state.driver ->
							do_reregister_driver(State#state.parking, State#state.parking, Driver);
						    true -> ok
					end,
					NewState = State#state{driver = Driver},
					if NewState#state.orderid =/= 0 ->							
							set_driver_online(Id, self(), NewState, 0),
							case get_order(NewState#state.orderid) of
								{Pid, _} -> 
									lager:info("login OK: state restored; order in progress; Driver: ~p Order: ~p~n",[Id,NewState#state.orderid]),
									Pid ! {driver_reconnect, self()},
									{ok, Driver, NewState};
								_ -> 
									lager:warning("possibly a bug: no order for disconnected driver with taken order; Order: ~p, Driver: ~p~n",[NewState#state.orderid, Driver#driver.id]),
									{ok, Driver, NewState#state{orderid = 0}}										 
							end;					   
						true ->
							Sts = case Driver#driver.state of
									  cant_work -> 
										lager:info("login OK: can't work; state restored; Driver: ~p~n",[Id]),
										negative_balance;
									  work -> 
										set_driver_online(Id, self(), NewState, 0),
										lager:info("login OK: state restored; Driver: ~p~n",[Id]),
										ok
								  end,
							{Sts, Driver, NewState#state{orderid = 0}}
					end
				end;
		[] -> 
			case ets:match_object(drivers_auth, #driver{id = Id, password = Pass, _='_'}) of
				[] -> 
					lager:info("login failed: wrong password; Driver: ~p~n",[Id]),
					fail;
				[Driver] when Driver#driver.state =:= cant_work -> 
					check_for_new_messages(Id),
					lager:info("login OK: cant work; Driver: ~p~n",[Id]),
					{negative_balance, Driver, 0};
				[Driver] when Driver#driver.state =:= work -> 
					check_for_new_messages(Id),
					lager:info("login OK; Driver: ~p~n",[Id]),
					NState = #state{driver = Driver, name = unreg},
					set_driver_online(Id, self(), NState, 0),
					set_driver_status(Id, free),
					{ok, Driver, NState}
			end
	end.

-spec get_driver_proc(integer()) -> pid()|no_driver.
%% get_driver_proc(Id) when is_integer(Id) ->
%%   case gproc:lookup_local_name({driver_id, Id}) of
%%     Pid when is_pid(Pid) -> Pid;
%%     undefined -> no_driver
%%   end.
get_driver_proc(Id) ->
	case ets:lookup(drivers_online, Id) of
		[{_, Pid, _, _}] when is_pid(Pid) -> Pid;
		_ -> no_driver
	end.

it_is_driver_proc(Id) when is_integer(Id) ->
  Name = {driver_id, Id},
  case gproc:lookup_local_name(Name) of
    undefined ->
      gproc:add_local_name(Name);
    _ -> ok
  end,
  ok.

set_driver_status(Driver, Status) when is_record(Driver, driver) ->
	set_driver_status(Driver#driver.id, Status);
set_driver_status(Id, Status) when is_integer(Id) ->
	if is_integer(Id) ->
			cache ! {status, Id, Status};
	   true -> ok
	end.

set_client_mode(Id, ClientMode) when is_atom(ClientMode) ->
	if is_integer(Id) ->
			cache ! {client_mode, Id, ClientMode};
	   true -> ok
	end.

set_gps(Id, Gps) ->
	if is_integer(Id) ->
			cache ! {gps, Id, Gps};
	   true -> ok
	end.


get_drivers_distrib() ->
	cache ! {get_distrib, self()},
	receive {distrib, Dist} -> Dist
	after 5000 -> 
			lager:info("waiting for distrib too long~n",[]),
			[{ParkingId, length(Orders), Drivers} || {ParkingId, Orders, Drivers} <- ets:tab2list(drivers_distrib)]
	end.

get_parkings_drivers(ParkingId) ->
	case ets:lookup(drivers_distrib, ParkingId) of 
		[{_, _, Drivers}] -> Drivers;
		_ -> []
	end.

get_parkings_orders(ParkingId) ->
	case ets:lookup(drivers_distrib, ParkingId) of 
		[{_, Orders, _}] -> Orders;
		_ -> []
	end.

do_register_driver(ParkingId, Driver) ->
	cache ! {register, ParkingId, Driver}.
do_unregister_driver(ParkingId, Driver) ->
	cache ! {unregister, ParkingId, Driver}.
do_reregister_driver(OldParking, NewParking, Driver) ->	
	cache ! {reregister, OldParking, NewParking, Driver}.

do_register_order(Order) ->
	cache ! {register_order, Order}.
do_unregister_order(Order) ->
	cache ! {unregister_order, Order}.

get_parkings() ->
	ets:tab2list(parkings).

get_rule(RuleHead) ->
	case ets:lookup(rules, RuleHead) of
		[{_, Rule}] -> 
			Rule;
		_ ->
			% lager:error("no rule for ~p~n", [RuleHead]),
			all
	end.

get_order(Id) ->
	case ets:lookup(orders, Id) of
		[{_, Pid, Order}] -> {Pid, Order};
		_ -> no_order
	end.

set_order_state(Order, Driver, Time, State) ->
	ets:insert(orders_states, {Order#order.id,							  
							   State,
							   Driver#driver.id,
							   Driver#driver.driverid,
							   time2code(Time)}).

set_unusual_order_state(Id) ->
	ets:insert(orders_states, {Id, unusual}).

set_forget_order_state(Id) ->
	ets:insert(orders_states, {Id, forget}).

do_remove_order(Id) ->
	ets:delete(orders, Id).

get_new_order(Id) ->
	case ets:lookup(orders, Id) of
		[{Id, _, Order}] -> Order;
		_ -> no_order
	end.

get_new_balance(Id) ->
	case ets:lookup(drivers_auth, Id) of
		[] -> 0;
		[D] when is_record(D, driver) ->
			D#driver.balance
	end.

%% to cache_cast
update_message(Mess)->
	%%lager:info("updating message#~p~n",[Id]),
	Id = Mess#message.id,
	Type = Mess#message.type,	
	if Type =/= announce -> 
			sql ! {update_message, {Id, 1}},
			if Type =/= lider ->
					ets:delete(messages, Id);
			   true ->
					ok %%nothing_to_del -lider
			end;
	   true ->
			ets:update_element(announces, Id, {8, cursecs2str(cursecs())}),   %UNSAFE_RECORD
			ets:update_counter(announces, Id, {5,1})   %UNSAFE_RECORD	   
	end.


-spec get_drivers_by_attr(integer()) -> [ #driver{} ].
get_drivers_by_attr(Attr) ->
	Cursec = cursecs(),
  lists:reverse(ets:foldl(fun({_Id, _Pid, State, _ }, Acc) ->
     case (driver:get_attr(State#state.driver) == Attr) of
      true ->
	      case Cursec - State#state.lastfinish >= autoappointment_pause() of
					false -> Acc;
          true -> [ State#state.driver | Acc ]
				end;
      false ->
        Acc
    end
  end, [], drivers_online)).

driver_gps(DriverId) ->
  case ets:lookup(drivers_online, DriverId) of
	  [{ _, Pid, State, _ }] when is_pid(Pid) ->
			State#state.gps;
		_ -> undefined
  end.

set_pause(DriverId, PauseEnd, IsWarningSend) when is_integer(DriverId) ->
	ets:insert(drivers_pause, { DriverId, PauseEnd, IsWarningSend }).

delete_pause(DriverId) ->
  ets:delete(drivers_pause, DriverId).

-spec orders( [integer()] ) -> [ #order{} ].
orders(OrderList) when is_list(OrderList) ->
	Orders = lists:map(fun(OrderId) ->
		case ets:lookup(orders, OrderId) of
			[{_, _, Order }] -> Order;
			_ -> undefined
		end
	end, OrderList),
	[ Order || Order <- Orders, is_record(Order, order) ].

run_orders_id() ->
	[ X || X <- [ State#state.orderid || {_, _, State, _} <- ets:tab2list(drivers_online), is_record(State, state) ], is_number(X), X > 0 ].

run_orders() ->
	orders(run_orders_id()).

-spec free_orders() -> [#order{}].
free_orders() ->
  RunOrders = run_orders_id(),
  ets:foldl(fun({Id, _Pid, Order}, Acc) ->
	  case lists:member(Id, RunOrders) of
			true -> Acc;
			false ->

				[ Order | Acc ]
	  end
	end, [], orders).

