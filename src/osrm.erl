-module(osrm).
-author("denilen").

-include("logger.hrl").

%% API
-export([distance/1,test/0]).

-define(OSRM_SERVER, "10.10.0.58:5000").

loc(Gps) ->
  io_lib:format("&loc=~f,~f", [ gps:lat(Gps),gps:lon(Gps) ]).

route_request(GpsList) when is_list(GpsList) ->
  LocList = lists:flatten([ loc(gps:from_scale(Gps)) || Gps <- GpsList ]),
  Url = lists:flatten("http://"++?OSRM_SERVER++"/viaroute?z=14&alt=false" ++ LocList),
  lager:info("OSRM Route request ~s", [ Url ]),
  { ok, {{_,200,_}, _, Body }} = httpc:request(get, { Url, [] },[],[]),
  jsx:decode(list_to_binary(Body)).

distance(GpsList) ->
  case gps:is_gps_list(GpsList) of
    true ->
      Response = route_request(GpsList),
      { ok, Result } = get_response_value([<<"route_summary">>, <<"total_distance">>], Response),
      Result;
    false ->
      undefined
  end.

get_response_value([], Answer) -> { ok, Answer };
get_response_value([Key|Path], Response) when is_list(Response) ->
  case proplists:get_value(Key, Response) of
    undefined -> undefined;
    Answer ->
      get_response_value(Path, Answer)
  end.

test() ->
  Gps1 = gps:to_scale({ 57.9848143566, 56.2655644682,0 }),
  Gps2 = gps:to_scale({ 58.004707487, 55.9477454467,0 }),
  distance([Gps1, Gps2]).
