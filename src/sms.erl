-module(sms).
-author("denilen").

-include("logger.hrl").

-export([send/3]).

-include_lib("./common.hrl").

%% API

send(Telephone, Text) ->
	send(undefined, Telephone, Text).

send(OrderId, Telephone, Text) when is_integer(OrderId); OrderId =:= undefined ->
	sql:qp("INSERT INTO SMS_OUT (order_id, tel_to, status, text, tries, createTime) VALUES ( #1, #2 ,'N', #3, 0, GETDATE() )", [
		case OrderId of
			Value when is_integer(Value) ->
				{ sql_integer, [ OrderId ] };
			undefined ->
				{ sql_integer, null }
		end,
		{ sql_varchar, [ Telephone ] },
		{ sql_varchar, [ Text ] }
	]),
  ok.
