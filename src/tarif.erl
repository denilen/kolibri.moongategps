-module(tarif).
-author("denilen").

-include("logger.hrl").

-include("tarif.hrl").

%% API
-export([
	type/1, create/2, is_valid/1,
	to_binary/2,
	type_to_integer/1, integer_to_type/1,
	ensure_valid/1,
	create_from_values/10
]).

-spec type(#tarif{}) -> tarif_type().
type(#tarif{ type = Type }) -> Type.

-spec create(Type :: tarif_type(), proplists:proplist()) -> #tarif{}.
create(Type, Properties) ->
  Keys = keys_list(Type),
  Prop = lists:filter(fun({Key, _}) -> lists:member(Key, Keys) end, Properties),
	Tarif = #tarif{
		type = Type,
		properties = Prop
	},
	ensure_valid(Tarif).

-spec ensure_valid(#tarif{}) -> #tarif{}.
ensure_valid(Tarif) ->
	case is_valid(Tarif) of
		true -> Tarif
	end.

-spec create_from_values(integer(), integer(), integer(), integer(), integer(), integer(), integer(), integer(), integer(), integer()) -> #tarif{}.
create_from_values(TarifType, TarifPauseTime, TarifPauseCost, TarifWaitTime, TarifWaitCost, TarifValue1, TarifValue2, TarifValue3, TarifValue4, TarifBaggageCost) ->
  DefaultTarif = [
    { pause_time, TarifPauseTime } ,
    { pause_cost, TarifPauseCost },
    { wait_time, TarifWaitTime },
    { wait_cost, TarifWaitCost},
	  { baggage_cost, TarifBaggageCost}
  ],
	case tarif:integer_to_type(TarifType) of
		none ->
			tarif:create(none, []);
		fixed ->
			tarif:create(fixed, [
        { full_cost, TarifValue1 }
				| DefaultTarif ]);
		distance ->
			tarif:create(distance, [
        { cost_before, TarifValue1},
        { distance_before, TarifValue2},
        { cost_after, TarifValue3 },
				{ time_before, TarifValue4 }
				| DefaultTarif ]);
		timed_minute ->
			tarif:create(timed_minute, [
        { cost_landing, TarifValue1 },
        { cost_minute, TarifValue2 },
				{ time_before, TarifValue3 },
				{ distance_before, TarifValue4 }
				| DefaultTarif ]);
		timed_hour ->
			tarif:create(timed_hour, [
        { cost_landing, TarifValue1 },
        { cost_minute, TarifValue2 },
				{ time_before, TarifValue3 },
				{ distance_before, TarifValue4 }
				| DefaultTarif ])
%% 		complex ->
%% 			tarif:create(complex, [
%%         { cost_before, TarifValue1 },
%%         { distance_before, TarifValue2 },
%%         { cost_after, TarifValue3 }
%% 				| DefaultTarif ])
  end.

is_valid(#tarif{} = Tarif) ->
	is_keys_defined(keys_list(type(Tarif)), Tarif#tarif.properties).

to_binary(Protocol, Tarif) ->
  <<
    (type_to_integer(Tarif)):8/unsigned-integer,
    (to_binary_frame(Protocol, Tarif))/binary
  >>.

to_binary_frame(_, #tarif{ type = none } = _Tarif) ->
  <<>>;
to_binary_frame(Protocol, #tarif{ type = fixed } = Tarif) ->
	<<
		(to_binary_standard(Protocol, Tarif))/binary,
		(get_value(full_cost, Tarif)):16/big-unsigned-integer
	>>;
to_binary_frame(Protocol, #tarif{ type = distance } = Tarif) ->
	<<
		(to_binary_standard(Protocol, Tarif))/binary,
		(get_value(cost_before, Tarif)):16/big-unsigned-integer,
		(get_value(distance_before, Tarif)):16/big-unsigned-integer,
		(get_value(cost_after, Tarif)):16/big-unsigned-integer,
		(get_value(time_before, Tarif)):16/big-unsigned-integer
	>>;
to_binary_frame(Protocol, #tarif{ type = timed_minute } = Tarif) ->
	<<
		(to_binary_standard(Protocol, Tarif))/binary,
		(get_value(cost_landing, Tarif)):16/big-unsigned-integer,
		(get_value(cost_minute, Tarif)):8/big-unsigned-integer,
		(get_value(time_before, Tarif)):16/big-unsigned-integer,
		(get_value(distance_before, Tarif)):8/big-unsigned-integer
	>>;
to_binary_frame(Protocol, #tarif{ type = timed_hour } = Tarif) ->
	<<
		(to_binary_standard(Protocol, Tarif))/binary,
		(get_value(cost_landing, Tarif)):16/big-unsigned-integer,
		(get_value(cost_minute, Tarif)):8/big-unsigned-integer,
		(get_value(time_before, Tarif)):16/big-unsigned-integer,
		(get_value(distance_before, Tarif)):8/big-unsigned-integer
	>>.
%%
%% to_binary_frame(#tarif{ type = complex } = Tarif) ->
%% 	<<
%% 		(to_binary_standard(Tarif))/binary,
%% 		(get_value(cost_before, Tarif)):8/big-unsigned-integer,
%% 		(get_value(distance_before, Tarif)):16/big-unsigned-integer,
%% 		(get_value(cost_after, Tarif)):8/big-unsigned-integer
%% 	>>.
%%
to_binary_standard(ProtocolVer, Tarif) ->
	case ProtocolVer of
		10 ->
			<<
				(get_value(pause_time, Tarif)):16/big-unsigned-integer,
				(get_value(pause_cost, Tarif)):8/big-unsigned-integer,
				(get_value(wait_time, Tarif)):8/big-unsigned-integer,
				(get_value(wait_cost, Tarif)):8/big-unsigned-integer,
				(get_value(baggage_cost, Tarif)):16/big-unsigned-integer
			>>;
		9 -> <<
			(get_value(pause_time, Tarif)):16/big-unsigned-integer,
			(get_value(pause_cost, Tarif)):8/big-unsigned-integer,
			(get_value(wait_time, Tarif)):8/big-unsigned-integer,
			(get_value(wait_cost, Tarif)):8/big-unsigned-integer
		>>

	end.

type_to_integer(Tarif) when is_record(Tarif, tarif) ->
  type_to_integer(type(Tarif));

type_to_integer(none) -> 0;
type_to_integer(fixed) -> 1;
type_to_integer(distance) -> 2;
type_to_integer(timed_minute) -> 3;
type_to_integer(timed_hour) -> 4;
%type_to_integer(complex) -> 5;
type_to_integer(_) -> 0.

integer_to_type(0) -> none;
integer_to_type(1) -> fixed;
integer_to_type(2) -> distance;
integer_to_type(3) -> timed_minute;
integer_to_type(4) -> timed_hour;
%integer_to_type(5) -> complex;
integer_to_type(_) -> none.

keys_list(none) ->
	[];
keys_list(fixed) ->
	[
		pause_time, pause_cost, wait_time, wait_cost, baggage_cost,
		full_cost
	];
keys_list(distance) ->
	[
		pause_time, pause_cost, wait_time, wait_cost, baggage_cost,
		cost_before, distance_before, cost_after, time_before
	];
keys_list(timed_minute) ->
	[
		pause_time, pause_cost, wait_time, wait_cost, baggage_cost,
		cost_landing, cost_minute, time_before, distance_before
	];
keys_list(timed_hour) ->
	[
		pause_time, pause_cost, wait_time, wait_cost, baggage_cost,
		cost_landing, cost_minute, time_before, distance_before
	].
%% keys_list(complex) ->
%% 	[
%% 		pause_time, pause_cost, wait_time, wait_cost,
%% 		cost_before, distance_before, cost_after
%% 	].

is_keys_defined(Keys, Proplists) ->
	lists:all(fun(Key) ->
		proplists:is_defined(Key, Proplists)
	end, Keys).

get_value(Key, Tarif) when is_record(Tarif, tarif) ->
	case proplists:get_value(Key, Tarif#tarif.properties) of
		undefined -> error(badarg, [ Key, Tarif ]);
		A -> A
	end.
