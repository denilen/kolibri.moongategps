-module(moongate_app).
-author("denilen").

-include("logger.hrl").

-behaviour(application).

-export([main/0, start/2, stop/1]).

-include_lib("./config.hrl").

main() ->
	lager:start(),
	%%application:start(lager),
	application:start(crypto),
	application:start(inets),
	application:start(gproc),
	application:start(odbc),
	ok = application:start(moongate).

%% Инициализация приложения.
start(_StartType, _StartArgs) ->

	io:format("~nMoonGate version ~p starting...~n",[version()]),
	io:format("~nCopyright (C) 2009-2015 Yakushev Denis, dennilen@gmail.com ~n",[]),
	io:format("------------------------------------------------------------~n",[]),

	%%error_logger:logfile({open, "./log/moongate.log"}),
	%%error_logger:tty(false),

	{ok, FilePath } = file:get_cwd(),

	{ok, Pid } = moongate_sup:start_link(FilePath ++ "/moongate.conf"),
	file:write_file(config:get(pid_file), os:getpid()),
	lager:info("Application started"),
	{ok, Pid}.

stop(_State) ->
	ok.
