-module(driver).
-author("denilen").

-include("logger.hrl").

-export([
	attr_has_autoappointment/1, has_attr/2, get_attr/1, get_rule_attr/1, gps/1,only_free_online/1,
	phone/1, log/2, log/3
]).

-include_lib("./common.hrl").
-include_lib("./rules.hrl").

%% API

attr_has_autoappointment(Driver) when is_record(Driver, driver) ->
  has_attr(?DRIVER_ATTR_AUTOAPPOINTMENT, Driver).

has_attr(Attr, Driver) ->
	Attr==get_attr(Driver).

get_attr(Driver) when is_record(Driver, driver) ->
	case Driver#driver.attribute_mode of
		undefined -> get_rule_attr(Driver#driver.attributes);
		Mode when Mode > 0, Mode < 255 -> Mode
	end.

get_rule_attr(Rule) when is_list(Rule) ->
	hd(lists:reverse(Rule)).

gps(Driver) ->
  cache:driver_gps(Driver#driver.id).

only_free_online(Drivers) ->
  lists:reverse(lists:foldl(fun(Driver, Acc) ->
	  case ets:lookup(drivers_online, Driver#driver.id) of
	    [{_Id, Pid, State, _ }] when is_pid(Pid), State#state.status =:= free, (State#state.name =:= unreg orelse State#state.name =:= main) ->
		    [ Driver | Acc ];
	    _ -> Acc
		end
	end, [], Drivers)).

phone(DriverId) when is_integer(DriverId) ->
  case sql:qp("SELECT d.Phones FROM Cars AS c JOIN Driver AS d ON c.DriverID = d.ID WHERE c.ID = #1", [
	  { sql_integer, [ DriverId ] }
  ]) of
		[ { Phone } ] -> Phone;
		_ -> undefined
	end.

log(Driver, Msg, Param) when is_record(Driver, driver) ->
	log(Driver#driver.id, Msg, Param);

log(DriverId, Msg, Param) ->
	Message = io_lib:format("Driver: ~p : " ++ Msg, [ DriverId | Param ]),
	lager:info(Message),
	Msg.

log(Driver, Msg) ->
	log(Driver, Msg, []).
