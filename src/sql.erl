-module(sql).
-author("denilen").

-compile(export_all).

-include("logger.hrl").

-include_lib("./config.hrl").
-include_lib("./common.hrl").


-ifdef(lic).
-define(SENS, erlang:process_flag(sensitive, true)). 
-define(CHECK, 
		First = case get(lic) of
					T when is_tuple(T)-> false;
					_ -> true
				end,
		if First ->
				timer:send_interval(check_period(), sql, check_lic);
			 true -> ok
		end,
		Lic = checker:check(),
		case Lic of 
			{_, _, NotDemo} ->
				if First and NotDemo ->
						{N,D,ND} = Lic,
						io:format("Checking license...~n",[]),
						io:format("unlimited license: ~p~n",[D]),
						if not D ->
								L = if ND -> N;
										 true -> demo_threshold()
									end,
								io:format("drivers limit: ~p~n",[L]);
							 true -> ok
						end;
					 true -> ok
				end,
				put(lic, Lic),
				if NotDemo -> 
						ok;
					 true -> %demo mode
						if First ->
								io:format("Demo mode enabled.~n",[]);
							 true ->
								io:format("Demo mode timed out; shutting down~n",[]),
								init:stop()
						end
				end;
			_ ->
				io:format("license get fail~n",[]),
				if First ->
						io:format("enabling demo mode~n",[]),
						put(lic, {0 ,false , false});
					 true ->
						case get(lic) of
							{_,_,false} ->
								io:format("Demo mode timed out; shutting down~n",[]),
								init:stop();
							_ ->
								io:format("enabling demo mode~n",[]),
								put(lic, {0 ,false , false})
						end
				end
			end).
			%io:format("------------------------------------------------------------~n",[])).

-else.
-define(SENS, ok).
-define(CHECK, ok).
-endif.

-ifdef(lic).
l2q({DriverThreshold, DisableThreshold, NotDemo}) ->
	if NotDemo ->
			if DisableThreshold ->
					" ";
				 true ->
					" top "++integer_to_list(DriverThreshold)++" "
			end;
		 true ->
			" top "++integer_to_list(demo_threshold())++" "
	end;
l2q(_) ->
	" ".
-else.
l2q(_) ->
	" ".
-endif.

start(PPid) ->	
	?SENS,
	erlang:register(sql, self()),
	process_flag(trap_exit, true),
	?CHECK,

	Pid = spawn_link (?MODULE, loop_start, [true]),
	Pid ! {lic, get(lic)},
	receive
		start_ok -> PPid ! start_ok;
		{'EXIT', Pid, Why} ->
			lager:error("DB conn problem: ~p~n", [Why]),
			io:format("DB conn problem: ~p~n", [Why]),
			erlang:exit({db_conn_problem, Why})
	end,
	super_loop(Pid).

super_loop(Pid) ->
	NewPid = receive
		{ call, FromPid, Ref, Msg } ->
			resend(Pid, { call, { FromPid, Ref }, Msg });
		Q when
				Q=:=refresh_auth;
				Q=:=refresh_parkings;
				Q=:=refresh_orders;
				Q=:=refresh_queue_rules ->
			resend(Pid, Q);
		{Q, List} when
				Q=:=flush_statuses;
				Q=:=flush_distrib;
				Q=:=flush_orders_states;
				Q=:=missing_client;
				Q=:=refresh_messages;
				Q=:=update_message;
				Q=:=update_announce ->
			resend(Pid, {Q, List});
		check_lic ->
			?CHECK,
			Pid ! {lic, get(lic)},
			Pid;
		X ->
			lager:error("Unknown sql message ~p", [ X ]),
			super_loop(Pid)
		end,
	?MODULE:super_loop(NewPid).

resend(Pid, What) ->
	Pid ! What,
	receive
		done -> Pid;
		{'EXIT', Pid, Why} ->
			lager:error("DB conn died: ~p~n", [Why]),
			NewPid = restart_loop(),
			resend(NewPid, What)
	end.

restart_loop() ->
	timer:sleep(loop_restart_timeout()),
	Pid = spawn_link(?MODULE, loop_start, [false]),
	Pid ! {lic, get(lic)},
	receive
		start_ok -> 
			lager:info("DB loop process restarted"),
			Pid;
		{'EXIT', Pid, Why} ->
			lager:error("DB conn died again: ~p~n", [Why]),
			restart_loop()
	end.

loop_start(First) ->
	?SENS,
	receive
		{lic, Lic} -> put(lic, Lic)
	end,
	db:start(),
	{ok,Conn} = db:connect(),
	lager:info("connected to DB: ~p~n",[Conn]),

	if First ->
			refresh_auth(Conn),
			timer:send_interval(auth_timeout(), sql, refresh_auth),

			refresh_parkings(Conn),
			timer:send_interval(parkings_timeout(), sql, refresh_parkings),

			refresh_queue_rules(Conn),
			timer:send_interval(rules_timeout(), sql, refresh_queue_rules),

			{updated, _} = db:q(Conn, "update lider set msgstatus = 1 where msgtype = 1 and msgstatus = 0 and msgdateend > getdate()"),
			LastId = refresh_messages(Conn, 0),
			erlang:send_after(messages_timeout(), sql, {refresh_messages, LastId}),

			{updated, _} = db:q(Conn, "update cars set status = 1024"),
			{updated, _} = db:q(Conn, "update orders set canal=0 where canal=1 and state = 0"),

			%%{selected, _, Res} = db:q(Conn, "select o.id, o.phone, o.addrfrom, o.housefrom, o.flatfrom, o.addrto, o.zoneid, o.dtarrive, o.descr, o.request_attributes, o.serviceid, o.sum, o.state, o.carid, o.time2meet, c.pass, c.state, c.serviceid, c.car_attributes, c.driverid from ORDERS as o join cars as c on c.id = o.carid where o.canal =1 and o.state =2"),
			{selected, _, Res} = db:q(Conn,
"SELECT o.id, o.phone, CASE WHEN (o.AddrFromName IS NULL OR o.AddrFromName = '') THEN o.AddrFrom ELSE '('+o.AddrFromName+') '+o.AddrFrom END AS addrfrom, o.housefrom, o.flatfrom,
CASE WHEN (o.AddrToName IS NULL OR o.AddrToName = '') THEN o.AddrTo ELSE '('+o.AddrToName+') '+ o.AddrTo END AS addrto,
o.zoneid, o.dtarrive, o.descr, o.request_attributes, o.serviceid, o.sum, o.state, o.carid, o.time2meet, c.pass, c.state, c.serviceid, c.car_attributes, c.driverid,
o.AddrFromGpsX, o.AddrFromGpsY, o.AddrFromGpsZ, d.Balance,
o.TarifType, o.TarifPauseTime, o.TarifPauseCost, o.TarifWaitTime, o.TarifWaitCost, o.TarifValue1, o.TarifValue2, o.TarifValue3, o.TarifValue4, o.TarifBaggageCost,
d.PauseTotalTime
FROM ORDERS as o
JOIN SERVICES s ON o.serviceid = s.id
JOIN cars AS c on c.id = o.carid
JOIN drivers AS d ON c.DriverID = d.ID WHERE o.canal=1 AND o.state=2"),
			RestartHackResult = lists:map(fun({Id, Phone, AddrFrom, HouseFrom, FlatFrom, AddrTo, Parking,
				DtArrive, Descr, RuleHead, ServiceId, Sum, State, CarId, Time2meet, Pass, CarState, CarServiceId, CarAttrib, DriverId,
				AddrFromGpsX, AddrFromGpsY, AddrFromGpsZ,
				Balance,
				TarifType, TarifPauseTime, TarifPauseCost, TarifWaitTime, TarifWaitCost, TarifValue1, TarifValue2, TarifValue3, TarifValue4, TarifBaggageCost,
				PauseTotalTime
			}) ->
				Driver = #driver{
					id = CarId,
					password = Pass,
					state = state2a(CarState),
					serviceid = CarServiceId,
					attributes = CarAttrib,
					driverid =  DriverId,
					balance = Balance,
					attribute_mode = driver:get_rule_attr(CarAttrib),
					pause_total = PauseTotalTime * 60
				},

				Order = #order{
					 id = Id,
					 phone = Phone,
					 addrfrom = AddrFrom,
					 housefrom = HouseFrom,
					 flatfrom = FlatFrom,
					 addrto = AddrTo,
					 parking = Parking,
					 dtarrive = DtArrive,
					 descr = Descr,
					 rule_head = RuleHead,
					 serviceid = ServiceId,
					 sum = Sum,
					 state = State,
					 gps_from = { AddrFromGpsX, AddrFromGpsY, AddrFromGpsZ },
					 tarif = tarif:create_from_values(TarifType, TarifPauseTime, TarifPauseCost, TarifWaitTime, TarifWaitCost, TarifValue1, TarifValue2, TarifValue3, TarifValue4, TarifBaggageCost)
				},
				{ Driver, Order, Time2meet }
			end, Res),
			cache ! {restart_hack, RestartHackResult},

			timer:send_interval(orders_timeout(), sql, refresh_orders);

		true -> ok
	end,	
	sql ! start_ok,
	?MODULE:loop(Conn).

loop(Conn) ->
	receive 
		refresh_auth ->
			refresh_auth(Conn);
		refresh_parkings ->
			refresh_parkings(Conn);
		refresh_orders ->
			refresh_orders(Conn, old),
			refresh_orders(Conn, predvar),
			refresh_orders(Conn, new);
		refresh_queue_rules ->
			refresh_queue_rules(Conn);
		{refresh_messages, LastId} ->
			NextId = refresh_messages(Conn, LastId),
			erlang:send_after(messages_timeout(), sql, {refresh_messages, NextId});

		{update_message, {MId, Count} = A } ->
			lager:info("updating message: ~p~n",[A]),
			if is_integer(Count), is_integer(MId) ->
				{updated, _} = db:q(Conn, "update lider set msgstatus = "++erlang:integer_to_list(Count)++" , msgdate = getdate() where id ="++erlang:integer_to_list(MId));
				true -> ok
			end;
		{update_announce, {AId, Count, _DReceive} = A } ->
			lager:info("updating announce; message: ~p~n",[A]),
			if  is_integer(Count), is_integer(AId) ->
					%{updated, _} = db:q(Conn, "update lider set msgstatus = "++erlang:integer_to_list(Count)++" , msgdate = '"++DReceive++"' where id ="++erlang:integer_to_list(AId))
					{updated, _} = db:q(Conn, "update lider set msgstatus = "++erlang:integer_to_list(Count)++" , msgdate = getdate() where id ="++erlang:integer_to_list(AId));
				true -> ok
			end;

		{flush_statuses, TupleList} ->
			flush_statuses(Conn, TupleList);

		{flush_distrib, List} ->
			flush_distrib(Conn, List);

		{flush_orders_states, List} ->
			flush_orders_states(Conn, List);

		{missing_client, {OrderId, CarId}} ->
			inform_missing_client(Conn, OrderId, CarId);
		{lic, Lic} ->
			put(lic, Lic);
		{ call, { FromPid, Ref }, Msg } ->
			Result = db:handle_call(Msg, Conn),
			FromPid ! { call_result, Ref, Result };
		X ->
			lager:error("Unknown sql message ~p. Programm error.", [ X ])
	end,
	sql ! done,
	?MODULE:loop(Conn).

refresh_orders(Conn, Part) ->
%	lager:info("selecting orders, part:~p conn:~p~n", [Part,Conn]),
	Cond = case Part of
				 predvar -> "o.state = 5 and o.canal = 0 and datediff(minute, getdate(), dtpredvar) < " ++ integer_to_list(predvar_time());
				 new -> "o.state != 5 and o.canal = 0";
				 old -> "o.canal = 1"
		end,
	{selected, _, Res} = db:q(Conn,
"SELECT o.id, o.phone, CASE WHEN (o.AddrFromName IS NULL OR o.AddrFromName = '') THEN o.AddrFrom ELSE '('+o.AddrFromName+') '+o.AddrFrom END AS addrfrom,
o.housefrom, o.flatfrom, CASE WHEN (o.AddrToName IS NULL OR o.AddrToName = '') THEN o.AddrTo ELSE '('+o.AddrToName+') '+ o.AddrTo END AS addrto,
o.zoneid, o.dtarrive, o.descr, o.request_attributes, o.serviceid, o.sum, o.state,
o.AddrFromGpsX, o.AddrFromGpsY, o.AddrFromGpsZ,
o.TarifType, o.TarifPauseTime, o.TarifPauseCost, o.TarifWaitTime, o.TarifWaitCost, o.TarifValue1, o.TarifValue2, o.TarifValue3, o.TarifValue4, o. TarifBaggageCost
FROM ORDERS AS o WHERE " ++ Cond),
%	lager:info("got ~p orders~n", [length(Res)]),
	List = lists:map(fun({Id, Phone, AddrFrom, HouseFrom, FlatFrom, AddrTo, Parking, DtArrive, Descr, RuleHead, ServiceId, Sum, State,
		AddrFromGpsX, AddrFromGpsY, AddrFromGpsZ,
		TarifType, TarifPauseTime, TarifPauseCost, TarifWaitTime, TarifWaitCost, TarifValue1, TarifValue2, TarifValue3, TarifValue4, TarifBaggageCost }) ->
      #order {
        id = Id,
        phone = Phone,
        addrfrom = AddrFrom,
        housefrom = HouseFrom,
        flatfrom = FlatFrom,
        addrto = AddrTo,
        parking = Parking,
        dtarrive = DtArrive,
        descr = Descr,
        rule_head = RuleHead,
        serviceid = ServiceId,
        sum = fix_int(Sum,0),
        state = State,
        gps_from = { AddrFromGpsX,AddrFromGpsY,AddrFromGpsZ },
        tarif = tarif:create_from_values(TarifType, TarifPauseTime, TarifPauseCost, TarifWaitTime, TarifWaitCost, TarifValue1, TarifValue2, TarifValue3, TarifValue4, TarifBaggageCost)
      }
    end, Res),
	if
		Part =/= old andalso length(List) >0 ->
			UpdateQuery = case Part of
							  predvar -> "update orders set canal = 1, state = 0 where id in (";
							  new -> "update orders set canal = 1 where id in ("
						  end,
			{updated, _} = db:q(Conn, UpdateQuery ++
										string:join([integer_to_list(element(1,Order)) || Order <- Res], ", ")
										++")"),
			cache ! {orders_new, List};
		Part =/= old andalso length(List) =:=0 -> wait_for_more;
		true -> cache ! {orders_old, List}
	end.
%	lager:info("done selecting orders~n").

refresh_auth(Conn) ->	
	lager:info("selecting cars~n"),
	{selected, _, Res} = db:q(Conn, "SELECT "++l2q(get(lic))++" c.id, c.pass, c.state, c.serviceid, c.car_attributes, c.driverid, cast(d.balance as int) AS balance, d.PauseTotalTime FROM cars AS c JOIN drivers AS d ON c.driverid = d.id JOIN services AS s ON s.ID = c.serviceid WHERE c.state IN (2,3)"),
	 List = [ #driver{
		id = Id,
		password = hex:hexstr_to_bin(Pass),
		state = state2a(State),
		serviceid = Service,
		attributes = Attributes,
		driverid = DriverId,
		balance = fix_int(Balance, 0),
		attribute_mode = driver:get_rule_attr(Attributes),
		pause_total = PauseTotalTime * 60
	} || {Id, Pass, State, Service, Attributes, DriverId, Balance, PauseTotalTime } <- Res],
	cache ! {auth, List}.
	%% lager:info("done selecting cars~n"),

refresh_parkings(Conn) ->
	lager:info("selecting zones~n"),
	{selected, _, Res} = db:q(Conn, "select id, descr from zones where rad=0 and state=0"),
	cache ! {parkings, Res}.
%% lager:info("done selecting zones~n"),

refresh_queue_rules(Conn) ->
	%%lager:info("selecting rules~n"),
	{selected, _, Res} = db:q(Conn, "select start_request, rules from queue_rules"),
	List = [{string:substr(Start, 1, length(Start)-1),
			 [sets:from_list(string:tokens(S,";")) || S <- string:tokens(Rule, ")(")]} 
			|| {Start, Rule} <- Res],
	cache ! {queue_rules, List}.
	%%lager:info("done selecting rules~n"),

flush_statuses(_Conn, { [], [], [] }) -> ok;
flush_statuses(Conn, { StatusList, ClientModeList, GpsList }) ->
	lager:info("Flush statuses: ~n StatusList = ~p ~n ClientModeList = ~p ~n GpsList = ~p", [ StatusList, ClientModeList, GpsList ]),
	%% TODO: Оптимизировать для построения одного SQL запроса.
	[db:qp(Conn, "UPDATE cars SET status = ? WHERE id = ?"
		,[
			{sql_integer, [a2status(S)]},
			{sql_integer, [D]}
		])
	 || {D, S} <- StatusList, is_integer(D)],

	[db:qp(Conn, "UPDATE cars SET client_mode = ? WHERE id = ?"
		,[
			{sql_integer, [client_mode_to_bin(ClientMode)]},
			{sql_integer, [D]}
		])
	 || {D, ClientMode} <- ClientModeList, is_integer(D)],

	[db:qp(Conn, "UPDATE cars SET Lon = ?, Lat = ? WHERE id = ?"
		,[
			{sql_integer, [X]},
			{sql_integer, [Y]},
			{sql_integer, [D]}
		])
	 || {D, { X, Y, _Z} } <- GpsList, is_integer(D)],
	 ok.

flush_distrib(Conn, List) ->
	%%lager:info("flushing driver distrib~n"),
	db:q(Conn,
									"update distrib set data = '["++
									string:join(
									[string:join([integer_to_list(I) 
													|| I <- [Parking
															 | [Driver#driver.id
																|| Driver <- Drivers]]]
												 , " ")
									 || {Parking, Drivers} <- List, length(Drivers) > 0]
									, "][")
									++ "]' where id=1").
	%lager:info("done flushing driver distrib~n").


flush_orders_states(Conn, List) ->
	%%lager:info("flushing order states~n"),
	lists:map(fun (Q) ->
						case Q of
							{Id, State, CarId, DriverId, Time2meet}->
								{St, R, TimeStr} = case State of
									 order_finished -> {3, 1, "dtend = getdate(), canal = 2"};
									 client_not_came -> { 3, 6, "dtend = getdate(), canal = 2"};
									 continue -> {2, 0, "dtbegin = getdate() "};
									 execution_timeout -> { 3, 2, "dtend = getdate()" }
								 end,
								if St =:= 2 ->
										do_notification(Conn, preliminary, Id, CarId);
									 true -> ok
								end,
								{updated, _} = db:qp(Conn, "update orders set state = ?, ResultCode = ? , carid = ?, driverid = ?, time2meet = ?, "++ TimeStr ++" where id = ?"
																, [{sql_integer, [St]},
																 {sql_integer, [R]},
																 {sql_integer, [CarId]},
																 {sql_integer, [DriverId]},
																 {sql_integer, [Time2meet]},
																 {sql_integer, [Id]}]);
							{Id, unusual} ->
								lager:info("unusual finalizing, possibly order cancelled ~p~n",[Id]),
								{updated, _} = db:q(Conn, "update orders set orders.canal = 2 where id = " ++ erlang:integer_to_list(Id));

							{Id, forget} ->
								lager:info("forget finalizing order ~p~n",[Id]),
								{updated, _} = db:q(Conn, "update orders set orders.canal = 0 where id = " ++ erlang:integer_to_list(Id))
						end
				end, List).
				%lager:info("done flushing order states~n").

inform_missing_client(Conn, OrderId, CarId) ->
	do_notification(Conn, missing, OrderId, CarId).

do_notification(Conn, Type, OrderId, CarId)->
	{Method, Mess, Nivr} = case Type of
						 preliminary -> {client_preliminary_notification_method(), " К вам под-т а-м ", "1"};
						 missing -> {client_missing_notification_method(), " Вас ожидает а-м ", "2"}
					 end,
	case Method of
		sms_phone -> db:q(Conn, "insert into SMS_OUT (order_id, tel_to, status, text, tries, createTime) select o.id, o.phone,  'N', 'Служба ' + o.fromPhone + '"++Mess++"' + c.Mark  + ' н-р '  + c.gosnum + ' в-ль ' + d.Phones, 0, GETDATE() from ORDERS as o , cars as c, drivers as d where o.ID = "++integer_to_list(OrderId)++" and c.ID = "++integer_to_list(CarId) ++ "and d.CarId = c.ID");
		sms_no_phone -> db:q(Conn, "insert into SMS_OUT (order_id, tel_to, status, text, tries, createTime) select o.id, o.phone,  'N', 'Служба ' + o.fromPhone + '"++Mess++"' + c.Mark  + ' номер '  + c.gosnum, 0, GETDATE() from ORDERS as o , cars as c, drivers as d where o.ID = "++integer_to_list(OrderId)++" and c.ID = "++integer_to_list(CarId) ++ "and d.CarId = c.ID");
		call -> db:q(Conn, "insert into CALL_OUT (order_id, car_id  , tel_to, status, nivr, tel_from, tries, start_time) select ID, "++integer_to_list(CarId)++" , phone, 'N', '"++Nivr++"', fromPhone, 0, GETDATE()  from ORDERS where ID = "++integer_to_list(OrderId));
		_ -> ok
	end.

refresh_messages(Conn, Id) ->
	%lager:info("selecting messages, starting from ~p~n",[Id]),
	{selected, _, Res} = db:q(Conn, "select id, carid, msgdatebegin, msgdateend, msgdate, msgtype, msgstatus, msg from lider where ((msgtype = 2 and msgdateend > getdate() and msgdatebegin < getdate()) or (msgtype != 2 and msgstatus = 0)) and id > "++erlang:integer_to_list(Id)++" order by id desc"),
	List = [{message, Idd, CarId, msgType2a(Type), fix_int(Status, 0), Begin, End, Date, Msg} ||
				 {Idd, CarId, Begin, End, Date, Type, Status, Msg} <- Res],  %UNSAFE_RECORD
	cache ! {messages, List},	
	case List of 
		[] -> Id;
		_ -> (hd(List))#message.id
	end.

forget_order(Conn, OrderId) when is_integer(OrderId) ->
	{updated, _} = db:qp(Conn, "UPDATE orders SET canal = 0 WHERE id = ?", [{sql_integer, [OrderId]}]),
	ok.


fix(Val, Pred, Default) ->
	B = Pred(Val),
	if B -> Val;
		 true -> Default
	end.

fix_int(Val, Default) ->
	fix(Val, fun erlang:is_integer/1, Default).

%% ----- ВСЕ ЧТО НАВЕРХУ - ПОДЛЕЖАЩЕЕ ВЫПИЛИВАНИЮ ------------

order_cash_save(OrderId, Payout, DistanceTotal, CashDistance, TimeTotal, CashTime, CashPauseTime, WaitTimeTotal, CashWaitTime, CashBaggage) ->
	{updated, _} = sql:update("orders" , "Payout = ?, DistanceTotal = ?, CashDistance = ?, TimeTotal = ?, CashTime = ?, CashPauseTime = ?, WaitTimeTotal = ?, CashWaitTime = ?, CashBaggage = ?",
		[
			{sql_integer,[Payout]},
			{sql_integer,[DistanceTotal]},
			{sql_integer,[CashDistance]},
			{sql_integer,[TimeTotal]},
			{sql_integer,[CashTime]},
			{sql_integer,[CashPauseTime]},
			{sql_integer,[WaitTimeTotal]},
			{sql_integer,[CashWaitTime]},
			{sql_integer,[CashBaggage]}
		],
		"id = ?",
		[
			{sql_integer, [OrderId]}
		]).

call(Msg, Timeout) ->
	Ref = make_ref(),
	sql ! { call, self(), Ref, Msg},
	receive
		{ call_result, Ref, Result } -> Result
	after Timeout ->
		{ error, timeout }
	end.

call(Msg) ->
	call(Msg, 5000).

q(Query) ->
	call({ query, Query}).

qp(Query, Param) ->
	call({ query_param, Query, Param}).

update(Table, SetSection, SetParam, WhereSection, WhereParam) ->
	call({ update, Table, SetSection, SetParam, WhereSection, WhereParam}).
