@echo off
title=MoonGate_{{node_title}}
set ERL_MAX_PORTS=20000
"{{erl_exe}}" -name "{{node_name}}" -setcookie "{{node_cookie}}" {{license}} -eval "canal:start()." -pa ebin -pa deps/gproc/ebin -pa deps/goldrush/ebin -pa deps/jsx/ebin -pa deps/lager/ebin
